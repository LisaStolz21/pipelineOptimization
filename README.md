# Application:

GamerAccountResource: https://localhost:8181/pipelineOptimization/restapi/account-service

ComparePopularityResource: https://localhost:8181/pipelineOptimization/restapi/comparison-service

Web-Application: https://localhost:8181/pipelineOptimization/jsf/pages/welcomePage.xhtml

## How to get Coverage Report for integration tests:

1. Deploy payara with VM option: -javaagent:C:
   \Users\STOL\IdeaProjects\pipelineOptimization\src\main\resources\coverage-reports\org.jacoco.agent-0.8.5-runtime.jar=append=false,output=tcpserver,port=10001
2. run mvn clean to clean target folder
3. reset jacoco.exec if necessary: coverage-reports/reset.bat (--reset reset execution data on test target after dump)
4. run mvn verify -PjacocoCoverageIT to execute all selenium and cucumber tests
5. get the Integration-Test-Coverage-Report from target/site/jacoco-profile-test

## Run Configurations:

Execute Integration tests with jacoco-coverage: mvn verify -PjacocoCoverageIT

## Statements for generating coverage report with the jacoco command line interface at runtime

final String commandJavaAgent = "java -javaagent:
src/main/resources/coverage-reports/org.jacoco.agent-0.8.5-runtime.jar=append=false,output=tcpserver,port=10001"; final
String commandDump = "java -jar src/main/resources/coverage-reports/org.jacoco.cli-0.8.5-nodeps.jar dump --address
127.0.0.1 --port 10001 --destfile target/jacoco-it.exec"; final String commandReport = "java -jar
src/main/resources/coverage-reports/org.jacoco.cli-0.8.5-nodeps.jar report target/jacoco-it.exec --classfiles
target/classes/de --sourcefiles src/main/java/ --html target/jacoco-report"; final String resetDump = "java -jar -jar
src/main/resources/coverage-reports/org.jacoco.cli-0.8.5-nodeps.jar dump --destfile= /target/jacoco-it.exec --port=10001
--reset";

Runtime.getRuntime().exec(resetDump); Runtime.getRuntime().exec(commandJavaAgent); Runtime.getRuntime().exec(
commandDump); Runtime.getRuntime().exec(commandReport);

### How to test jacoco Command Line Interface manually -> jacoco cli

1. **Deploy payara with VM option**:
   -javaagent:C:
   \Users\STOL\IdeaProjects\pipelineOptimization\src\main\resources\coverage-reports\org.jacoco.agent-0.8.5-runtime.jar=append=false,output=tcpserver,port=10001

2. **Test Application**:
   mvn verify

3. **Dump Data into jacoco exec**
   java -jar src/main/resources/coverage-reports/org.jacoco.cli-0.8.5-nodeps.jar dump --address 127.0.0.1 --port 10001
   --destfile target/jacoco-it.exec

4. **Read Coverage**
    - In Intellij: Run -> Show Coverage Data -> jacoco-it.exec
    - Report: java -jar src/main/resources/coverage-reports/org.jacoco.cli-0.8.5-nodeps.jar report target/jacoco-it.exec
      --classfiles target/classes/de --sourcefiles src/main/java/ --html target/jacoco-report --csv test

### Use the jacoco.agent.jar for manuel deployment

bin\asadmin start-domain selectiveExecution

bin\asadmin create-jvm-options -javaagent:
${com.sun.aas.instanceRoot}/lib/ext/org.jacoco.agent.jar=append=false,output=tcpserver,port=10001

bin\asadmin restart-domain selectiveExecution

bin\asadmin deploy pipelineOptimization.war