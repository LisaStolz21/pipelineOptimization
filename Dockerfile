
FROM payara/micro

LABEL maintainer="lisa.stolz@live.de" app="pipelineOptimization"

COPY ./target/pipelineOptimization.war /usr/local/payara/bin/pipelineOptimization.war
