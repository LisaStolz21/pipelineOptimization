package de.ee.optimization;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class EnvironmentConfiguration {

    public static final String PROP_API_URL = "api.test.url";
    public static final String PROP_WEB_URL = "selenium.test.url";
    public static final String PROP_WEBDRIVER = "selenium.webdriver";
    private static final Logger logger = LogManager.getLogger(EnvironmentConfiguration.class);

    public enum Environment {LOCAL, PROD}

    public static Environment getEnvironment() {
        final String environmentName = System.getProperty("environment");
        if (environmentName == null) {
            logger.warn("Environment configuration not found, please make sure that the system property is configured ");
        }
        if (Environment.LOCAL.name().equals(environmentName)) {
            return Environment.LOCAL;
        } else if (Environment.PROD.name().equals(environmentName)) {
            return Environment.PROD;
        } else {
            throw new IllegalStateException("Environment not known: " + environmentName);
        }
    }

    public static String getSystemProperty(final String propertyValue) {
        final String systemProp = System.getProperty(propertyValue);
        if (systemProp == null) {
            logger.warn("Could not find System Property: " + propertyValue);
        }
        return systemProp;
    }

    public static boolean isProdEnvironment() {
        return Environment.PROD.equals(getEnvironment());
    }
}
