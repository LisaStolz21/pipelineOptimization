package de.ee.optimization.integrationtest.control;

import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * helper Class for testing {@link de.ee.optimization.restapi.ComparePopularityResource} rest interface
 */
public class ComparePopularityControl {


    public String getComparisonMessage() {
        final String path = "getComparisonMessage";
        final WebTarget webTarget = ApiTestSupport.createWebTarget(ApiTestSupport.popularityComparisonApiPath, path);
        final Response clientResponse = webTarget.request().get();
        ApiTestSupport.testStatus(clientResponse, 200);
        return clientResponse.readEntity(String.class);
    }


}
