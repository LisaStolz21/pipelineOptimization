package de.ee.optimization.integrationtest.control;

import de.ee.optimization.game.entity.AccountRegistration;
import de.ee.optimization.game.entity.GamerAccount;
import de.ee.optimization.game.entity.RegistrationState;

import javax.ws.rs.client.Entity;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

/**
 * helper Class for testing {@link de.ee.optimization.restapi.GamerAccountResource} rest interface
 */
public class AccountManagementControl {

    public GamerAccount createGamerAccount(final String firstName, final String lastName, final String mail, final String userId) {
        final String methodPath = "createGamerAccount/";

        final AccountRegistration accountRegistration = new AccountRegistration(firstName, lastName, mail, userId);
        accountRegistration.setRegistrationState(RegistrationState.VERIFIED);

        final WebTarget webTarget = ApiTestSupport.createWebTarget(ApiTestSupport.accountManagementApiPath, methodPath);
        final Response clientResponse = webTarget.request().post(Entity.entity(accountRegistration, MediaType.APPLICATION_JSON_TYPE));
        ApiTestSupport.testStatus(clientResponse, 200);
        return clientResponse.readEntity(GamerAccount.class);
    }

    public void saveRegistration(final String firstName, final String lastName, final String mail, final String userId) {
        final String methodPath = "saveUserAccountRegistration/" + firstName + "/" + lastName + "/" + mail + "/" + userId;

        final WebTarget webTarget = ApiTestSupport.createWebTarget(ApiTestSupport.accountManagementApiPath, methodPath);
        final Response clientResponse = webTarget.request().put(Entity.json(""));
        ApiTestSupport.testStatus(clientResponse, 200);
    }

    public void verifyRegistration(final String mail) {
        final String methodPath = "verifyUserAccountRegistration/" + mail;

        final WebTarget webTarget = ApiTestSupport.createWebTarget(ApiTestSupport.accountManagementApiPath, methodPath);
        final Response clientResponse = webTarget.request().put(Entity.json(""));
        ApiTestSupport.testStatus(clientResponse, 200);
    }

    public GamerAccount findGamerAccountByName(final String userName) {
        final String path = "findGamerAccountByName/" + userName;
        final WebTarget webTarget = ApiTestSupport.createWebTarget(ApiTestSupport.accountManagementApiPath, path);
        final Response clientResponse = webTarget.request().get();
        ApiTestSupport.testStatus(clientResponse, 200);
        return clientResponse.readEntity(GamerAccount.class);
    }

    public void deleteUser(final String mail) {
        final String path = "deleteUser/" + mail;
        final WebTarget webTarget = ApiTestSupport.createWebTarget(ApiTestSupport.accountManagementApiPath, path);
        final Response clientResponse = webTarget.request().delete();
        ApiTestSupport.testStatus(clientResponse, 200);
    }

}
