package de.ee.optimization.integrationtest.control;

import de.ee.optimization.EnvironmentConfiguration;

import javax.ws.rs.client.Client;
import javax.ws.rs.client.ClientBuilder;
import javax.ws.rs.client.WebTarget;
import javax.ws.rs.core.Response;

/**
 * support Class for testing rest interface
 */
public class ApiTestSupport {

    public static final String accountManagementApiPath = "/account-service";
    public static final String popularityComparisonApiPath = "/comparison-service";

    public static WebTarget createWebTarget(final String servicePath, final String methodPath) {
        final Client client = ClientBuilder.newClient();
        return client.target(getApiUrl()).path(servicePath).path(methodPath);
    }

    public static void testStatus(final Response clientResponse, final int status) {
        if (clientResponse.getStatus() != status) {
            throw new RuntimeException("the clients response status was " + clientResponse.getStatus()
                    + " we expected the client to receive status " + status);
        }
    }

    private static String getApiUrl() {
        return EnvironmentConfiguration.getSystemProperty(EnvironmentConfiguration.PROP_API_URL);
    }
}
