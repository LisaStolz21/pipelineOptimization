package de.ee.optimization.restapi;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;

@Path("/comparison-service")
public class ComparePopularityResource {

    @GET
    @Produces("text/plain")
    @Path("/getComparisonMessage")
    public String getComparisonMessage() {
        return "Information about the most popular Games in 2021!";
    }
}
