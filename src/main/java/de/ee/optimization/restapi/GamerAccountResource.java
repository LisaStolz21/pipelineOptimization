package de.ee.optimization.restapi;

import de.ee.optimization.game.boundary.GamesBF;
import de.ee.optimization.game.control.GamesRepository;
import de.ee.optimization.game.entity.AccountRegistration;
import de.ee.optimization.game.entity.GamerAccount;
import de.ee.optimization.game.entity.RegistrationState;

import javax.ejb.Stateless;
import javax.inject.Inject;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.util.Date;
import java.util.Optional;

@Stateless
@Path("/account-service")
@Produces(value = {MediaType.TEXT_PLAIN})
public class GamerAccountResource {

    @Inject
    public GamesBF gamesBF;

    @Inject
    private GamesRepository gamesRepository;

    @POST
    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/createGamerAccount")
    public Response createGamerAccount(final AccountRegistration accountRegistration) {
        final Optional<AccountRegistration> registration = gamesBF.findUserRegistration(accountRegistration.getMailAddress());
        if (!registration.isPresent()) {
            return Response.status(Response.Status.NOT_FOUND).entity("user registration not found").build();
        }
        checkForExistingAccountAndUpdateExpiryDate(registration.get());
        GamerAccount gamerAccount = null;
        if (RegistrationState.VERIFIED.equals(registration.get().getRegistrationState())) {
            gamerAccount = gamesBF.createGamerAccount(registration.get());
        }
        return gamerAccount != null ? Response.ok(gamerAccount).build() : Response.status(Response.Status.NOT_MODIFIED).build();
    }

    @PUT
    @Path("/saveUserAccountRegistration/{firstName}/{lastName}/{mail}/{accountName}")
    public Response saveRegistration(@PathParam("firstName") final String firstName,
                                     @PathParam("lastName") final String lastName,
                                     @PathParam("mail") final String mail, @PathParam("accountName") final String accountName) {
        for (final AccountRegistration accountRegistration : gamesBF.findAllAccountRegistrations()) {
            if (accountRegistration.getFirstName().equals(firstName) && accountRegistration.getLastName().equals(lastName)) {
                return Response.status(Response.Status.NOT_FOUND).entity("user already registered").build();
            }
            if (accountRegistration.getAccountName().equals(accountName)) {
                return Response.status(Response.Status.FORBIDDEN).entity("The username is already taken, please choose another one").build();
            }
        }
        final AccountRegistration userRegistration = gamesBF.saveNewUserRegistration(firstName, lastName, mail, accountName);
        if (userRegistration == null) {
            return Response.status(Response.Status.NOT_MODIFIED).entity("").build();
        }
        return Response.ok().build();
    }

    @PUT
    @Path("/verifyUserAccountRegistration/{mail}")
    public Response verifyUserAccountRegistration(@PathParam("mail") final String mail) {
        final Optional<AccountRegistration> registration = gamesBF.findUserRegistration(mail);
        if (registration.isPresent()) {
            //verify it
            registration.get().setRegistrationState(RegistrationState.VERIFIED);
        }
        return Response.ok().build();
    }

    @GET
    @Produces(MediaType.APPLICATION_JSON)
    @Path("/findGamerAccountByName/{userName}")
    public Response findGamerAccountByName(@PathParam("userName") final String userName) {
        final Optional<GamerAccount> gamerAccount = gamesBF.findGamerAccountByName(userName);
        return Response.ok(gamerAccount.orElse(null)).build();
    }

    @DELETE
    @Path("/deleteUser/{mail}")
    public Response deleteUser(@PathParam("mail") final String mail) {
        final Optional<AccountRegistration> registration = gamesBF.findUserRegistration(mail);
        if (registration.isPresent()) {
            final Optional<GamerAccount> account = gamesBF.findGamerAccountByName(registration.get().getAccountName());
            account.ifPresent(gamerAccount -> gamesRepository.remove(gamerAccount));
            gamesRepository.remove(registration.get());
        }
        return Response.ok().build();
    }

    private void checkForExistingAccountAndUpdateExpiryDate(final AccountRegistration accountRegistration) {
        final Optional<GamerAccount> gamerAccountOptional = gamesBF.findGamerAccountByName(accountRegistration.getAccountName());
        final Date today = new Date();
        if (gamerAccountOptional.isPresent()) {
            if (gamerAccountOptional.get().getExpirationDate().before(today)) {
                gamerAccountOptional.get().setExpirationDate(today);
                gamesBF.extendGamerAccountExpirationDate(gamerAccountOptional.get(), 6);
            }
            Response.status(Response.Status.NOT_FOUND).entity("Account already exists").build();
        }
    }
}
