package de.ee.optimization;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.util.concurrent.TimeUnit;

public class TimeUtils {


    public static long getDurationLocalDateTime(final LocalDateTime start, final LocalDateTime end) {
        return end.toInstant(ZoneOffset.UTC).toEpochMilli() - start.toInstant(ZoneOffset.UTC).toEpochMilli();
    }

    public static void waitSeconds(final int seconds) {
        try {
            Thread.sleep(TimeUnit.SECONDS.toMillis(seconds));
        } catch (final InterruptedException e) {
            e.printStackTrace();
        }
    }
}
