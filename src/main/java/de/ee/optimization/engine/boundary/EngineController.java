package de.ee.optimization.engine.boundary;

import de.ee.optimization.engine.entity.EngineInfo;
import de.ee.optimization.engine.entity.WowExpansion;

import javax.annotation.PostConstruct;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Named
@ViewScoped
public class EngineController implements Serializable {
    private final transient List<EngineInfo> engineInfos = new ArrayList<EngineInfo>();
    private List<WowExpansion> wowExpansionsList;

    @Inject
    ExpansionsBF expansionsBF;


    @PostConstruct
    public void init() {
        prepareData();
    }

    private void prepareData() {
        wowExpansionsList = new ArrayList<WowExpansion>();
        wowExpansionsList.addAll(expansionsBF.findAllExpansions());

        engineInfos.add(prepareEngineInfo("Unity", "San Francisco", "7 Days to Die", "Hearthstone", "Pokémon Go"));
        engineInfos.add(prepareEngineInfo("Unreal", "North Carolina", "Fortnite", "Gears of War", "BioShock"));
        engineInfos.add(prepareEngineInfo("Cry", "Frankfurt", "Far Cry", "Sniper: Ghost Warrior 2", "Warface"));
        engineInfos.add(prepareEngineInfo("Frostbite", "Stockholm", "Battlefield 3", "Star Wars Battlefront", "Need for Speed"));

    }

    private EngineInfo prepareEngineInfo(final String engineName, final String headOffice, final String... gameNames) {
        final List<String> gamesOfEngine = new ArrayList<String>(Arrays.asList(gameNames));
        final EngineInfo info = new EngineInfo(engineName, headOffice, 4);
        info.setReleasedGames(gamesOfEngine);
        return info;
    }

    public List<EngineInfo> getDataInput() {
        return engineInfos;
    }

    public List<WowExpansion> getWowExpansionsList() {
        return wowExpansionsList;
    }

}
