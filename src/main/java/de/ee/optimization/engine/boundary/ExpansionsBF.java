package de.ee.optimization.engine.boundary;

import de.ee.optimization.engine.control.ExpansionsRepository;
import de.ee.optimization.engine.entity.WowExpansion;

import javax.ejb.Stateless;
import javax.inject.Inject;
import java.util.List;

@Stateless
public class ExpansionsBF {

    @Inject
    private ExpansionsRepository expansionsRepository;

    /**
     * Find all {@link WowExpansion}s
     *
     * @return A list of {@link WowExpansion}s
     */
    public List<WowExpansion> findAllExpansions() {
        return expansionsRepository.findAllExpansions();
    }

    /**
     * Find all {@link WowExpansion}s by type
     *
     * @param type the expansion type
     * @return A list of {@link WowExpansion}s
     */
    public List<WowExpansion> findAllExpansionsByType(final String type) {
        return expansionsRepository.findExpansionsByType(type);
    }

}
