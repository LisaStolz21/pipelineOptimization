package de.ee.optimization.engine.entity;

import java.util.List;

public class EngineInfo {

    private final String engineName;
    private final String headOffice;
    private final int monthlyPayment;
    private List<String> releasedGames;

    public EngineInfo(final String engineName, final String headOffice, final int monthlyPayment) {
        this.engineName = engineName;
        this.headOffice = headOffice;
        this.monthlyPayment = monthlyPayment;
    }

    /**
     * Gets the engineName.
     *
     * @return value of engineName
     */
    public String getEngineName() {
        return engineName;
    }

    /**
     * Gets the headOffice.
     *
     * @return value of headOffice
     */
    public String getHeadOffice() {
        return headOffice;
    }

    /**
     * Gets the monthlyPayment.
     *
     * @return value of monthlyPayment
     */
    public int getMonthlyPayment() {
        return monthlyPayment;
    }

    /**
     * Gets the releasedGames.
     *
     * @return value of releasedGames
     */
    public List<String> getReleasedGames() {
        return releasedGames;
    }

    /**
     * Sets the releasedGames.
     *
     * @param releasedGames value of releasedGames
     */
    public void setReleasedGames(final List<String> releasedGames) {
        this.releasedGames = releasedGames;
    }
}
