package de.ee.optimization.engine.entity;

public class EngineGame {

    private final String engineName;
    private final String gameName;
    private final String releaseDate;

    public EngineGame(final String engineName, final String gameName, final String releaseDate) {
        this.engineName = engineName;
        this.gameName = gameName;
        this.releaseDate = releaseDate;
    }

    /**
     * Gets the engineName.
     *
     * @return value of engineName
     */
    public String getEngineName() {
        return engineName;
    }

    /**
     * Gets the gameName.
     *
     * @return value of gameName
     */
    public String getGameName() {
        return gameName;
    }

    /**
     * Gets the releaseDate.
     *
     * @return value of releaseDate
     */
    public String getReleaseDate() {
        return releaseDate;
    }
}
