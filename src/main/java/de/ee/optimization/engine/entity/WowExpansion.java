package de.ee.optimization.engine.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import java.io.Serializable;

@Entity
@Table(name = "wow_expansion")
@NamedQueries({@NamedQuery(name = WowExpansion.NQ_FIND_ALL_EXPANSIONS, query = "SELECT exp FROM WowExpansion exp"),
        @NamedQuery(name = WowExpansion.NQ_FIND_EXPANSIONS_BY_TYPE, query =
                "SELECT exp FROM WowExpansion exp WHERE exp.expansionType = :" + WowExpansion.EXPANSION_TYPE)
        ,})
public class WowExpansion implements Serializable {

    public static final String NQ_FIND_ALL_EXPANSIONS = "expansions.findAll";
    public static final String NQ_FIND_EXPANSIONS_BY_TYPE = "expansions.findAllByType";
    public static final String EXPANSION_TYPE = "expansionType";

    @Id
    @Column(name = "expansion", length = 50, columnDefinition = "VARCHAR(50)")
    private String expansion;

    @Column(name = "expansion_type", length = 50, columnDefinition = "VARCHAR(50)")
    private String expansionType;

    @Column(name = "release", length = 50, columnDefinition = "VARCHAR(50)")
    private String release;


    public WowExpansion() {
    }

    public WowExpansion(final String expansion, final String expansionType, final String release) {
        this.expansion = expansion;
        this.expansionType = expansionType;
        this.release = release;
    }

    /**
     * Gets the expansion.
     *
     * @return value of expansion
     */
    public String getExpansion() {
        return expansion;
    }

    /**
     * Sets the expansion.
     *
     * @param expansion value of expansion
     */
    public void setExpansion(final String expansion) {
        this.expansion = expansion;
    }

    /**
     * Gets the expansionType.
     *
     * @return value of expansionType
     */
    public String getExpansionType() {
        return expansionType;
    }

    /**
     * Sets the expansionType.
     *
     * @param expansionType value of expansionType
     */
    public void setExpansionType(final String expansionType) {
        this.expansionType = expansionType;
    }

    /**
     * Gets the release.
     *
     * @return value of release
     */
    public String getRelease() {
        return release;
    }

    /**
     * Sets the release.
     *
     * @param release value of release
     */
    public void setRelease(final String release) {
        this.release = release;
    }
}
