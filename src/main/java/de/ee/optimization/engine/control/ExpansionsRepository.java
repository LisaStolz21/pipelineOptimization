package de.ee.optimization.engine.control;

import de.ee.optimization.engine.entity.WowExpansion;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;

@Stateless
public class ExpansionsRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public void persist(final Object o) {
        entityManager.persist(o);
    }

    public List<WowExpansion> findAllExpansions() {
        final TypedQuery<WowExpansion> namedQuery = entityManager.createNamedQuery(WowExpansion.NQ_FIND_ALL_EXPANSIONS, WowExpansion.class);
        return namedQuery.getResultList();
    }

    public List<WowExpansion> findExpansionsByType(final String expansionType) {
        final TypedQuery<WowExpansion> namedQuery = entityManager.createNamedQuery(WowExpansion.NQ_FIND_EXPANSIONS_BY_TYPE, WowExpansion.class);
        namedQuery.setParameter(WowExpansion.EXPANSION_TYPE, expansionType);
        return namedQuery.getResultList();
    }


    public void remove(final Object o) {
        if (o != null) {
            entityManager.remove(o);
        }
    }
}
