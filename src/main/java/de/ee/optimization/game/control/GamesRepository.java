package de.ee.optimization.game.control;

import de.ee.optimization.game.entity.AccountRegistration;
import de.ee.optimization.game.entity.Game;
import de.ee.optimization.game.entity.GamerAccount;
import de.ee.optimization.game.entity.RegistrationState;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import java.util.List;
import java.util.Optional;

@Stateless
public class GamesRepository {

    @PersistenceContext
    private EntityManager entityManager;

    @TransactionAttribute(TransactionAttributeType.MANDATORY)
    public void persist(final Object o) {
        entityManager.persist(o);
    }

    public List<Game> findAllGames() {
        final TypedQuery<Game> namedQuery = entityManager.createNamedQuery(Game.NQ_FIND_ALL_GAMES, Game.class);
        return namedQuery.getResultList();
    }

    public List<GamerAccount> findAllGamerAccounts() {
        final TypedQuery<GamerAccount> namedQuery = entityManager.createNamedQuery(GamerAccount.NQ_FIND_ALL_GAMER,
                GamerAccount.class);
        return namedQuery.getResultList();
    }

    public Optional<GamerAccount> findGamerByName(final String userId) {
        final TypedQuery<GamerAccount> namedQuery = entityManager.createNamedQuery(GamerAccount.NQ_FIND_GAMER_BY_NAME
                , GamerAccount.class);
        namedQuery.setParameter(GamerAccount.GAMERS_USER_ID, userId);
        return namedQuery.getResultList().stream().findFirst();
    }

    public List<AccountRegistration> findAccountRegistrationByState(final RegistrationState state) {
        final TypedQuery<AccountRegistration> namedQuery = entityManager.createNamedQuery(AccountRegistration.NQ_FIND_BY_STATE
                , AccountRegistration.class);
        namedQuery.setParameter(AccountRegistration.REGISTRATION_STATE, state.getName());
        return namedQuery.getResultList();
    }

    public List<AccountRegistration> findAllAccountRegistrations() {
        final TypedQuery<AccountRegistration> namedQuery = entityManager.createNamedQuery(AccountRegistration.NQ_FIND_ALL
                , AccountRegistration.class);
        return namedQuery.getResultList();
    }

    public Optional<AccountRegistration> findRegistrationByMail(final String mail) {
        final TypedQuery<AccountRegistration> namedQuery = entityManager.createNamedQuery(AccountRegistration.NQ_FIND_BY_MAIL, AccountRegistration.class);
        namedQuery.setParameter(AccountRegistration.PARAM_MAIL, mail);
        return namedQuery.getResultList().stream().findFirst();
    }

    public void remove(final Object o) {
        if (o != null) {
            entityManager.remove(o);
        }
    }
}
