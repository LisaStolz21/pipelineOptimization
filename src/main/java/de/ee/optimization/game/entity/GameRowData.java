package de.ee.optimization.game.entity;

public class GameRowData {

    private final String gameName;
    private final String genre;
    private final PlatformPackage platform;
    private final String developer;
    private final String release;
    private String selectedPlatform;
    private boolean isSelected;


    public GameRowData(final String gameName, final String genre, final PlatformPackage platform,
                       final String developer, final String release) {
        this.gameName = gameName;
        this.genre = genre;
        this.platform = platform;
        this.developer = developer;
        this.release = release;
    }

    public boolean isSelected() {
        return isSelected;
    }

    public void setSelected(final boolean selected) {
        isSelected = selected;
    }

    public String getGameName() {
        return gameName;
    }

    public PlatformPackage getPlatform() {
        return platform;
    }

    public String getGenre() {
        return genre;
    }

    public String getDeveloper() {
        return developer;
    }

    public String getRelease() {
        return release;
    }

    public String getSelectedPlatform() {
        return selectedPlatform;
    }

    public void setSelectedPlatform(final String selectedPlatform) {
        this.selectedPlatform = selectedPlatform;
    }
}
