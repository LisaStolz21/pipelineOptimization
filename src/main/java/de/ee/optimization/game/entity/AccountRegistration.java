package de.ee.optimization.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import java.io.Serializable;

@Entity
@Table(name = "account_registration", uniqueConstraints = {@UniqueConstraint(columnNames = {"mail_address"})})
@NamedQueries({@NamedQuery(name = AccountRegistration.NQ_FIND_ALL, query = "SELECT AR FROM AccountRegistration AR"),
        @NamedQuery(name = AccountRegistration.NQ_DELETE_REFUSED_REGISTRATIONS, query = "DELETE FROM AccountRegistration AR WHERE AR.registrationState = :" + AccountRegistration.REFUSED_STATE),
        @NamedQuery(name = AccountRegistration.NQ_FIND_BY_MAIL, query =
                "SELECT AR FROM AccountRegistration AR WHERE AR.mailAddress = :" + AccountRegistration.PARAM_MAIL),
        @NamedQuery(name = AccountRegistration.NQ_FIND_BY_STATE, query =
                "SELECT AR FROM AccountRegistration AR WHERE AR.registrationState = :" + AccountRegistration.REGISTRATION_STATE)
        ,})
public class AccountRegistration implements Serializable {

    public static final String NQ_FIND_ALL = "query.findAll";
    public static final String NQ_FIND_BY_STATE = "query.findByRegistrationState";
    public static final String NQ_FIND_BY_MAIL = "query.findRegistrationByMail";
    public static final String NQ_DELETE_REFUSED_REGISTRATIONS = "query.deleteRefusedUserRegistrations";
    public static final String REGISTRATION_STATE = "state";
    public static final String REFUSED_STATE = "REFUSED";
    public static final String PARAM_MAIL = "mail";

    @Id
    @Column(name = "id", length = 50, columnDefinition = "VARCHAR(50)")
    private String id;

    @Column(name = "first_name", length = 50, columnDefinition = "VARCHAR(50)")
    private String firstName;

    @Column(name = "last_name", length = 50, columnDefinition = "VARCHAR(50)")
    private String lastName;

    @Column(name = "mail_address", length = 25, columnDefinition = "VARCHAR(25)")
    private String mailAddress;

    @Column(name = "account_name", length = 50, columnDefinition = "VARCHAR(50)")
    private String accountName;

    @Column(name = "status", nullable = false, columnDefinition = "VARCHAR(25)")
    @Enumerated(EnumType.STRING)
    private RegistrationState registrationState;

    public AccountRegistration(final String firstName, final String lastName, final String mailAddress, final String accountName) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.mailAddress = mailAddress;
        this.accountName = accountName;
    }

    public AccountRegistration() {

    }


    public String getId() {
        return id;
    }

    public void setId(final String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(final String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getAccountName() {
        return accountName;
    }

    public void setAccountName(final String accountName) {
        this.accountName = accountName;
    }

    public RegistrationState getRegistrationState() {
        return registrationState;
    }

    public void setRegistrationState(final RegistrationState registrationState) {
        this.registrationState = registrationState;
    }
}
