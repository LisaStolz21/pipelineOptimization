package de.ee.optimization.game.entity;

public enum RegistrationState {
    CHECK,
    VERIFIED,
    REFUSED,
    REACTIVATED;

    public String getName() {
        return name();
    }
}
