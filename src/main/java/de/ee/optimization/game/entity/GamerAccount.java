package de.ee.optimization.game.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import java.io.Serializable;
import java.util.Date;

@Entity
@Table(name = "gamer_account")
@NamedQueries({@NamedQuery(name = GamerAccount.NQ_FIND_ALL_GAMER, query = "SELECT GA FROM GamerAccount GA"),
        @NamedQuery(name = GamerAccount.NQ_FIND_GAMER_BY_NAME, query =
                "SELECT GA FROM GamerAccount GA WHERE GA.userId = :" + GamerAccount.GAMERS_USER_ID)
        ,})
public class GamerAccount implements Serializable {

    public static final String NQ_FIND_ALL_GAMER = "gamer.findAll";
    public static final String NQ_FIND_GAMER_BY_NAME = "gamer.findByName";
    public static final String GAMERS_USER_ID = "userId";

    @Id
    @Column(name = "user_id", length = 50, columnDefinition = "VARCHAR(50)")
    private String userId;

    @Column(name = "first_name", length = 50, columnDefinition = "VARCHAR(50)")
    private String firstName;

    @Column(name = "last_name", length = 50, columnDefinition = "VARCHAR(50)")
    private String lastName;

    @Temporal(TemporalType.DATE)
    @Column(name = "registration_date", nullable = false)
    private Date registrationDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "expiration_date", nullable = false)
    private Date expirationDate;

    @Column(name = "mail_address", length = 25, columnDefinition = "VARCHAR(25)")
    private String mailAddress;

//    @ManyToMany(cascade = CascadeType.ALL)
//    @JoinTable(name = "games_users_mapping", joinColumns = @JoinColumn(name = "user_id"),
//            inverseJoinColumns = @JoinColumn(name = "game_name"))
//    private final Set<Game> associatedGames;

    public GamerAccount(final String userId, final String firstName, final String lastName, final String mail) {
        this.userId = userId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.mailAddress = mail;
//        associatedGames = new HashSet<Game>();

    }

    public GamerAccount() {
//        associatedGames = new HashSet<Game>();
    }

    public void setUserId(final String userId) {
        this.userId = userId;
    }

    public String getUserId() {
        return userId;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(final String firstName) {
        this.firstName = firstName;
    }

    public Date getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(final Date registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getMailAddress() {
        return mailAddress;
    }

    public void setMailAddress(final String mailAddress) {
        this.mailAddress = mailAddress;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(final String lastName) {
        this.lastName = lastName;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void setExpirationDate(final Date expirationDate) {
        this.expirationDate = expirationDate;
    }

//    public void associateGamesToUser(final List<Game> games) {
//        associatedGames.addAll(games);
//    }
//
//    @JsonIgnore
//    public Set<Game> getAssociatedGames() {
//        return associatedGames;
//    }
}
