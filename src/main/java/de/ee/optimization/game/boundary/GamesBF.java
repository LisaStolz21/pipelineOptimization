package de.ee.optimization.game.boundary;

import de.ee.optimization.game.control.GamesRepository;
import de.ee.optimization.game.entity.AccountRegistration;
import de.ee.optimization.game.entity.Game;
import de.ee.optimization.game.entity.GamerAccount;
import de.ee.optimization.game.entity.RegistrationState;

import javax.ejb.Stateless;
import javax.ejb.TransactionAttribute;
import javax.ejb.TransactionAttributeType;
import javax.inject.Inject;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.UUID;
import java.util.logging.Logger;

@Stateless
public class GamesBF {

    private static final Logger logger = Logger.getLogger(Logger.GLOBAL_LOGGER_NAME);

    @Inject
    private GamesRepository gamesRepository;


    public List<Game> findAllGames() {
        return gamesRepository.findAllGames();
    }

    public List<GamerAccount> findAllGamerAccounts() {
        return gamesRepository.findAllGamerAccounts();
    }

    public Optional<GamerAccount> findGamerAccountByName(final String userId) {
        return gamesRepository.findGamerByName(userId);
    }

    /**
     * Find all {@link AccountRegistration}s for a specific registration state.
     *
     * @param state the registration state {@link RegistrationState}
     * @return A list of {@link AccountRegistration}s
     */
    public List<AccountRegistration> findAccountRegistrationsByState(final RegistrationState state) {
        return gamesRepository.findAccountRegistrationByState(state);
    }

    /**
     * Find all {@link AccountRegistration}s
     *
     * @return A list of {@link AccountRegistration}s
     */
    public List<AccountRegistration> findAllAccountRegistrations() {
        return gamesRepository.findAllAccountRegistrations();
    }

    /**
     * Find optional {@link AccountRegistration} for a specific user by mail.
     *
     * @param mail the unique mail address
     * @return optional {@link AccountRegistration}
     */
    public Optional<AccountRegistration> findUserRegistration(final String mail) {
        return gamesRepository.findRegistrationByMail(mail);
    }


    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public GamerAccount createGamerAccount(final AccountRegistration accountRegistration) {
        final GamerAccount gamerAccount = new GamerAccount(accountRegistration.getAccountName(),
                accountRegistration.getFirstName(), accountRegistration.getLastName(), accountRegistration.getMailAddress());
        gamerAccount.setRegistrationDate(new Date());
        gamerAccount.setExpirationDate(new Date());
//        extendGamerAccountExpirationDate(gamerAccount, 12);
        gamesRepository.persist(gamerAccount);
        return gamerAccount;
    }

    @TransactionAttribute(TransactionAttributeType.REQUIRES_NEW)
    public AccountRegistration saveNewUserRegistration(final String firstName, final String lastName, final String mail, final String accountName) {

        final AccountRegistration registration = new AccountRegistration(firstName, lastName, mail, accountName);
        registration.setId(UUID.randomUUID().toString());
        registration.setRegistrationState(RegistrationState.CHECK);
        gamesRepository.persist(registration);
        return registration;
    }


    public void extendGamerAccountExpirationDate(final GamerAccount account, final int numberOfMonths) {
        final LocalDate now = LocalDate.now();
        account.setExpirationDate(convert(now.plusMonths(numberOfMonths)));
        gamesRepository.persist(account);
    }

    public Date convert(final LocalDate date) {
        return Date.from(date.atStartOfDay(ZoneId.systemDefault()).toInstant());
    }

}
