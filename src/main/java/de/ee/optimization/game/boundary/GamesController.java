package de.ee.optimization.game.boundary;

import de.ee.optimization.game.entity.*;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.context.FacesContext;
import javax.faces.view.ViewScoped;
import javax.inject.Inject;
import javax.inject.Named;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Named
@ViewScoped
public class GamesController implements Serializable {

    @Inject
    GamesBF gamesBF;

    @Inject
    @Chosen
    LocalDateTime localDateTime;

    private final transient List<GameRowData> rowDataInput = new ArrayList<GameRowData>();

    @PostConstruct
    public void init() {
        prepareData();
    }

    private void prepareData() {
        final List<GameRowData> shownRowData = new ArrayList<GameRowData>();
        final List<Game> allGames = gamesBF.findAllGames();
        for (final Game game : allGames) {
            final GameRowData rowData = new GameRowData(game.getName(), game.getGamesGenre().getName(),
                    game.getPlatform(), game.getDeveloper(), getReleaseDate(game.getRelease()));
            rowData.setSelected(false);
            rowData.setSelectedPlatform(getPlatformsString(game.getPlatform()).get(0));
            shownRowData.add(rowData);
        }
        rowDataInput.addAll(shownRowData);
    }


    public String getReleaseDate(final Date releaseDate) {
        return SimpleDateFormat.getDateInstance().format(releaseDate);
    }

    public List<String> getPlatformsString(final PlatformPackage platformPackage) {
        final Set<Platform> platformSet = PlatformMapper.getPlatforms(platformPackage);
        final List<String> listOfPlatforms = new ArrayList<String>();
        for (final Platform platform : platformSet) {

            listOfPlatforms.add(platform.getName());
        }
        return listOfPlatforms;
    }

    public String getLocalDateTimeString() {
        final DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd.MM.yyyy");
        return localDateTime.format(dtf);
    }

    public void showMsgGameSelectionSubmitted() {
        String testMsg = "";
        for (final GameRowData rowData : getRowDataInput()) {
            if (rowData.isSelected()) {
                testMsg =
                        "We received your Game selection: Game: " + rowData.getGameName() + " Platform: " + rowData.getSelectedPlatform();
                FacesContext.getCurrentInstance().addMessage(null, new FacesMessage(FacesMessage.SEVERITY_INFO, "",
                        testMsg));
            }
        }
    }

    public List<GameRowData> getRowDataInput() {
        return rowDataInput;
    }

}