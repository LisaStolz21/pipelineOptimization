package de.ee.selectiveexec.boundary;

import de.ee.selectiveexec.entity.TestLevel;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.Description;
import org.junit.runner.Result;

import java.lang.annotation.Annotation;
import java.lang.reflect.Method;

/**
 * Listener implementation to measure the per test coverage for selenium tests
 * this Listener can be used for every JUnit based Test
 */
public class SeleniumCoverageListener extends AbstractCoverageListener {

    private static final TestLevel testlevel = TestLevel.UI_TEST;

    String currentTestName = "";
    int executedTestCaseNmb = 0;
    int testCaseCountOfCurrentTest = 0;

    private void init(final Description description) {
        currentTestName = description.getTestClass().getSimpleName();
        testCaseCountOfCurrentTest = getNumberOfTestCasesForClass(description.getTestClass());
    }

    /**
     * called before the execution of selenium tests has been started
     * this method initializes the name and number of test cases for the first Test
     *
     * @param description the description contains information about the first Test to execute
     */
    @Override
    public void testRunStarted(final Description description) {
        testExecutionStarted(testlevel);
        init(description.getChildren().get(0));
    }

    /**
     * called when a single Test is going to be executed
     * this method initializes the name and number of the current test
     *
     * @param description the description contains information about the first Test to execute
     */
    @Override
    public void testStarted(final Description description) {
        System.out.println("starting execution of " + description.getMethodName());
        final String testName = description.getTestClass().getSimpleName();
        if (!testName.equals(currentTestName)) {
            init(description);
        }
    }

    /**
     * called before the execution of selenium tests has been started
     */
    @Override
    public void testFinished(final Description description) {
        executedTestCaseNmb++;
        if (executedTestCaseNmb == testCaseCountOfCurrentTest) {
            testCasesFinished(currentTestName, testlevel);
            executedTestCaseNmb = 0;
        }
    }

    /**
     * called when all selenium tests have been executed
     * publish just created selenium-result-mapping.xml when all selenium tests where executed
     */
    @Override
    public void testRunFinished(final Result result) {
        testsFinished(testlevel);
    }

    /**
     * determines the number of test cases of a test class by using java reflection
     * counts the number methods that are annotated with @Test and not @Ignore
     */
    private int getNumberOfTestCasesForClass(final Class cls) {
        int numberOfTestCases = 0;
        try {
            for (final Method method : cls.getDeclaredMethods()) {
                final Annotation[] annotations = method.getDeclaredAnnotations();
                for (final Annotation annotation : annotations) {
                    if (annotation instanceof Test && !(annotation instanceof Ignore)) {
                        numberOfTestCases++;
                    }
                }
            }
            return numberOfTestCases;
        } catch (final Throwable e) {
            e.printStackTrace();
        }
        return numberOfTestCases;
    }
}
