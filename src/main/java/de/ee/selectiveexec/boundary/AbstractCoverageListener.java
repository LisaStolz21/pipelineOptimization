package de.ee.selectiveexec.boundary;

import de.ee.optimization.EnvironmentConfiguration;
import de.ee.selectiveexec.control.JacocoCmdHelper;
import de.ee.selectiveexec.control.VersionControlManager;
import de.ee.selectiveexec.control.VersionControlSynchronizer;
import de.ee.selectiveexec.control.XmlDocumentSupport;
import de.ee.selectiveexec.entity.CoverageCommandLineOptions;
import de.ee.selectiveexec.entity.TestLevel;
import org.apache.logging.log4j.LogManager;
import org.junit.runner.notification.RunListener;
import org.w3c.dom.Document;

import java.nio.file.Path;
import java.nio.file.Paths;

import static de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider.getFileName;
import static de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider.getPath;
import static de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider.getPathToFile;
import static de.ee.selectiveexec.entity.ConstantValues.TEXT_RED;
import static de.ee.selectiveexec.entity.ConstantValues.TEXT_RESET;

/**
 * Custom implementation for Junit RunListener to enable the measurement of per test code coverage
 * The Coverage Data gets saved after the execution of all test cases of an testClass
 */
public abstract class AbstractCoverageListener extends RunListener {

    private static final org.apache.logging.log4j.Logger logger = LogManager.getLogger(AbstractCoverageListener.class);

    protected void testCasesFinished(final String testName, final TestLevel level) {
        logger.info(TEXT_RED + "all test cases of " + testName + " were executed, writing coverage data now" + TEXT_RESET);
        createCoverageReportForSingleTest(testName, level);
    }

    protected void testExecutionStarted(final TestLevel level) {
        if (TestLevel.API_TEST.equals(level)) {
            logger.info("Starting execution of Cucumber Tests - Coverage Analysis per Feature is active");
        } else if (TestLevel.UI_TEST.equals(level)) {
            logger.info("Starting execution of Selenium Tests - Coverage Analysis per Test is active");
        }
    }

    protected void testsFinished(final TestLevel level) {
        logger.info(TEXT_RED + "all test were executed, publishing coverage mapping " + TEXT_RESET);
        if (EnvironmentConfiguration.isProdEnvironment()) {
//            publishCreatedCoverageMapping(level);
            gitPublishFile(level);
        }
    }

    /**
     * executes cmd statements to determine the coverage data.
     * the collected data is used to create a mapping between test and relevant Java Classes
     * the mapping will be stored in xml file
     */
    private void createCoverageReportForSingleTest(final String testName, final TestLevel level) {
        final CoverageCommandLineOptions commandLineOptions = getCommandLineOptionsForTestName(testName);
        JacocoCmdHelper.dump(commandLineOptions);
        JacocoCmdHelper.report(commandLineOptions);
        JacocoCmdHelper.store(testName, commandLineOptions, level);
        JacocoCmdHelper.reset(commandLineOptions);
    }

    /**
     * publishes the test coverage mapping that was just created to the repository of the branch in which it was created.
     *
     * @param testLevel to find the name and the path of the file to be published
     */
    private void publishCreatedCoverageMapping(final TestLevel testLevel) {
        final String nameOfCurrentBranch = EnvironmentConfiguration.getSystemProperty("branchName");
//        final String systemPropRepoUrl = EnvironmentConfiguration.getSystemProperty("remote.repository.url");
        final String pathToGit = "/home/gitlab-runner/pipelineOptRepo";
        final String pathToFile = getPathToFile(testLevel);
        final String fileName = getFileName(testLevel);
        final String saveDirectory = getPath(testLevel);

        final VersionControlSynchronizer vcs = VersionControlSynchronizer.initializeSynchronizer(nameOfCurrentBranch, pathToGit, true, true);
        vcs.gitCheckoutAndRebase();
        final Document document = XmlDocumentSupport.getXmlDocumentByPath(pathToFile);
        if (document != null) {
            XmlDocumentSupport.saveXmlFile(document, pathToFile);
            final Path directoryToSave = vcs.getRepositoryPath().resolve(saveDirectory);
            XmlDocumentSupport.saveXmlFile(document, directoryToSave.toString() + "/" + fileName);

            System.out.println("getDirectPath " + vcs.getRepositoryPath().toString());
            System.out.println("getPathToSaveXml " + directoryToSave.toString());
            System.out.println("path manual " + directoryToSave.toString() + "/" + fileName);
            System.out.println("path  " + pathToFile);
        }

        vcs.gitAddFile(saveDirectory, fileName);
        vcs.gitCommit();
        vcs.gitPush(true);
    }

    public void gitPublishFile(final TestLevel level) {
        final String fileName;
        final String pathToFile;
        if (TestLevel.API_TEST.equals(level)) {
            fileName = "cucumber-result-mapping.xml";
            pathToFile = "src/main/resources/coverage-reports/cucumber-result-mapping.xml";
        } else {
            fileName = "selenium-result-mapping.xml";
            pathToFile = "src/main/resources/coverage-reports/selenium-result-mapping.xml";
        }
        final String branchName = "version-control-implementation";
        final String pathToGit = "/home/gitlab-runner/pipelineOptRepo";
        final Path directory = Paths.get(pathToGit);
        VersionControlManager.gitCheckoutBranch(directory, branchName);
        final Path directoryToSave = directory.resolve("src/main/resources/coverage-reports");

        final Document document = XmlDocumentSupport.getXmlDocumentByPath(pathToFile);
        if (document != null) {
            XmlDocumentSupport.saveXmlFile(document, directoryToSave.toString() + "/" + fileName);
        }
        VersionControlManager.gitAdd(directoryToSave, fileName);
        VersionControlManager.gitCommit(directoryToSave);
        VersionControlManager.gitPush(directoryToSave);
    }

    private CoverageCommandLineOptions getCommandLineOptionsForTestName(final String testName) {
        final CoverageCommandLineOptions commandLineOptions = CoverageCommandLineOptionsProvider.getCoverageCommandLineOptions();
        final String nameOfExecutable = testName + "-executable.exec";
        commandLineOptions.setDestFile(CoverageCommandLineOptionsProvider.createOption(commandLineOptions.getDestFile().getOption(),
                "target/" + nameOfExecutable));
        commandLineOptions.setCsv(CoverageCommandLineOptionsProvider.createOption(commandLineOptions.getCsv().getOption(),
                "target/" + testName + ".csv"));
        return commandLineOptions;
    }

}
