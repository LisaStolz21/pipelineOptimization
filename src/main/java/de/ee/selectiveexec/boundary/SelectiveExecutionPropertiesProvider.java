package de.ee.selectiveexec.boundary;

import de.ee.selectiveexec.entity.TestLevel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.StringJoiner;

/**
 * Provider for selective execution properties file
 * the file contains the relevant test name and will be used to configure maven failsafe plugin executions
 * <p>
 * the file gets updated in {@link de.ee.selectiveexec.control.GitDiffReader} after identifying
 * different classes List between current branch and master branch
 * there should be a default value for test execution where all tests are contained
 */
public class SelectiveExecutionPropertiesProvider {

    private static final String SELECTIVE_EXEC_PROPERTIES_FILE = "maven/selective-execution.properties";
    private static final String COVERAGE_RESULTS_SUFFIX = "coverage.results";
    private static final String CALCULATED_TESTS_SUFFIX = "tests.to.execute";

    public static Properties loadProperties() {
        try {
            final Properties properties = new Properties();

            final File file = new File(SELECTIVE_EXEC_PROPERTIES_FILE);
            properties.load(new FileInputStream(file));
            return properties;
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static String getPropertyValue(final String propertyKey) {
        final Properties properties = loadProperties();
        return properties.get(propertyKey).toString();
    }

    public static String getSelectiveExecPropertiesPath() {
        return SELECTIVE_EXEC_PROPERTIES_FILE;
    }

    private static String identifyPropertyKey(final String testKey, final String suffix) {
        return new StringJoiner(".").add(testKey).add("coverage").add("results").add(suffix).toString();
    }

    public static String getFileName(final TestLevel level) {
        return getPropertyValue(buildPropertyKey(level.getKey(), COVERAGE_RESULTS_SUFFIX, "file"));
    }

    public static String getPath(final TestLevel level) {
        return getPropertyValue(buildPropertyKey(level.getKey(), COVERAGE_RESULTS_SUFFIX, "path"));
    }

    public static String getPathToFile(final TestLevel level) {
        return getPath(level) + "/" + getFileName(level);
    }

    public static String getTestExecutionList(final TestLevel level, final boolean needKey) {
        final String propertyKey = buildPropertyKey(level.getKey(), CALCULATED_TESTS_SUFFIX);
        return needKey ? propertyKey : getPropertyValue(propertyKey);
    }

    public static String getDefaultTestExecutionList(final TestLevel level, final boolean needKey) {
        final String propertyKey = buildPropertyKey(level.getKey(), CALCULATED_TESTS_SUFFIX, "default");
        return needKey ? propertyKey : getPropertyValue(propertyKey);
    }

    private static String buildPropertyKey(final String testKey, final String... values) {
        final StringJoiner joiner = new StringJoiner(".").add(testKey);
        for (final String value : values) {
            joiner.add(value);
        }
        return joiner.toString();
    }

}
