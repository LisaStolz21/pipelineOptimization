package de.ee.selectiveexec.boundary;

import de.ee.selectiveexec.entity.TestLevel;
import io.cucumber.plugin.EventListener;
import io.cucumber.plugin.event.EventPublisher;
import io.cucumber.plugin.event.TestRunFinished;
import io.cucumber.plugin.event.TestRunStarted;
import io.cucumber.plugin.event.TestSourceParsed;
import io.cucumber.plugin.event.TestSourceRead;

/**
 * Listener implementation to measure the per test coverage for cucumber features
 */
public class CucumberCoverageListener extends AbstractCoverageListener implements EventListener {

    private static final TestLevel testlevel = TestLevel.API_TEST;
    String currentTestName = "";
    String currentFeaturePath = "";

    @Override
    public void setEventPublisher(final EventPublisher publisher) {
        publisher.registerHandlerFor(TestSourceRead.class, this::featureRead);
        publisher.registerHandlerFor(TestSourceParsed.class, this::featureParsed);
        publisher.registerHandlerFor(TestRunStarted.class, this::runStarted);
        publisher.registerHandlerFor(TestRunFinished.class, this::runFinished);
    }

    /**
     * called before the execution of cucumber tests has been started
     */
    private void runStarted(final TestRunStarted event) {
        testExecutionStarted(testlevel);
    }

    /**
     * called when all cucumber tests have been executed
     * publish just created cucumber-result-mapping.xml when all cucumber tests where executed
     */
    private void runFinished(final TestRunFinished event) {
        testsFinished(testlevel);
    }

    /**
     * called before the execution of Scenarios of a cucumber feature started
     */
    private void featureRead(final TestSourceRead event) {
        currentTestName = getFeatureName(event.getUri().getPath());
        currentFeaturePath = getFeaturePath(event.getUri().getPath());
    }

    /**
     * called when all Scenarios of a cucumber feature were tested
     */
    private void featureParsed(final TestSourceParsed event) {
        testCasesFinished(currentFeaturePath, testlevel);
        currentTestName = getFeatureName(event.getUri().getPath());
        currentFeaturePath = getFeaturePath(event.getUri().getPath());
    }

    private static String getFeatureName(final String path) {
        String line = path;
        final String substringFeature = "/features";
        line = line.substring(line.indexOf(substringFeature) + substringFeature.length() + 1);
        return line.replace(".", "-");
    }

    private static String getFeaturePath(final String path) {
        String line = path;
        final String substringFeature = "/pipelineOptimization";
        line = line.substring(line.indexOf(substringFeature) + substringFeature.length() + 1);
        return line;
    }
}
