package de.ee.selectiveexec.boundary;

import de.ee.selectiveexec.entity.CoverageCommandLineOptions;
import de.ee.selectiveexec.entity.OptionPair;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

/**
 * Provider for jacoco cmd options properties file
 */
public class CoverageCommandLineOptionsProvider {

    private static final String CONFIG_PROPERTIES_FILE_PATH = "src/main/resources/coverage-reports/jacoco-configuration-param.properties";

    private static final String classFilesOption = "classfiles";
    private static final String sourceFilesOption = "sourcefiles";
    private static final String destFileOption = "destfile";
    private static final String addressOption = "address";
    private static final String portOption = "port";
    private static final String htmlOption = "html";
    private static final String csvOption = "csv";
    private static final String resetOption = "reset";
    private static final String nameOption = "name";
    private static final String appendOption = "append";
    private static final String outputOption = "output";

    public static Properties loadProperties() {
        try {
            final Properties properties = new Properties();

            final File file = new File(CONFIG_PROPERTIES_FILE_PATH);
            properties.load(new FileInputStream(file));
            return properties;
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static CoverageCommandLineOptions getCoverageCommandLineOptions() {
        final CoverageCommandLineOptions options = new CoverageCommandLineOptions();
        final Properties properties = loadProperties();
        options.setAddress(getOptionValue(addressOption, properties));
        options.setDestFile(getOptionValue(destFileOption, properties));
        options.setPort(getOptionValue(portOption, properties));
        options.setClassFiles(getOptionValue(classFilesOption, properties));
        options.setSourceFiles(getOptionValue(sourceFilesOption, properties));
        options.setCsv(getOptionValue(csvOption, properties));
        options.setHtml(getOptionValue(htmlOption, properties));
        options.setName(getOptionValue(nameOption, properties));
        options.setAppend(new OptionPair(appendOption, properties.get(appendOption).toString()));
        return options;
    }

    public static List<String> getOptionsPrepare() {
        final Properties properties = loadProperties();
        final List<String> options = new ArrayList<String>();
        options.add(getPrepareOption(portOption, properties));
        options.add(getPrepareOption(appendOption, properties));
        options.add(getPrepareOption(outputOption, properties));
        return options;
    }

    private static OptionPair getOptionValue(final String option, final Properties properties) {
        final String optionValue = " --" + option + " " + properties.get(option).toString();
        final OptionPair optionPair = new OptionPair(option, optionValue);
        return optionPair;
    }

    private static String getPrepareOption(final String option, final Properties properties) {
        return option + "=" + properties.get(option).toString();
    }

    public static OptionPair createOption(final String option, final String value) {
        final String optionValue = " --" + option + " " + value;
        System.out.println("new option " + option + " with value (" + optionValue + ") created");
        return new OptionPair(option, optionValue);
    }
}
