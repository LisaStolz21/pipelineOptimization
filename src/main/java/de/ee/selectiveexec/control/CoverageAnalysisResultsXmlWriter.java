package de.ee.selectiveexec.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.xml.sax.SAXException;

import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Map;

public class CoverageAnalysisResultsXmlWriter {

    private static final String XML_ROOT_TEST_CLASSES_MAPPING = "TestNameCoveredClassesMapping";
    private static final String XML_ELEMENT_TEST_NAME = "TestName";
    private static final String XML_ELEMENT_CLASS = "Class";

    private static final Logger logger = LogManager.getLogger(CoverageAnalysisResultsXmlWriter.class);

    public void createOrUpdateXmlByCoverageReportMap(final Map<String, List<String>> testNameByCoveredClassesMap, final String pathToXml) {
        final Document document;
        try {
            final File xmlFile = new File(pathToXml);
            if (xmlFile.exists()) {
                document = XmlDocumentSupport.getDocumentBuilder().parse(xmlFile);
            } else {
                document = XmlDocumentSupport.getDocumentBuilder().newDocument();
                XmlDocumentSupport.createXmlRootElement(document, XML_ROOT_TEST_CLASSES_MAPPING);
            }
            createXmlDocumentForCoverageReportMap(document, testNameByCoveredClassesMap);
            XmlDocumentSupport.saveXmlFile(document, pathToXml);

        } catch (final IOException | ParserConfigurationException | SAXException e) {
            logger.error("error occurred while trying to write the coverage mapping to xml file ", e);
        }
    }

    public void createXmlDocumentForCoverageReportMap(final Document doc, final Map<String, List<String>> testNameByCoveredClassesMap) {
        for (final String testName : testNameByCoveredClassesMap.keySet()) {
            XmlDocumentSupport.removeNodeByValueAndTagName(doc, testName, XML_ELEMENT_TEST_NAME);

            final Element testNameElement = XmlDocumentSupport.createXmlChildOfRootElement(doc, XML_ELEMENT_TEST_NAME, testName);
            testNameElement.setAttribute("testName", testName);
            final List<String> affectedClasses = testNameByCoveredClassesMap.get(testName);
            for (final String className : affectedClasses) {
                XmlDocumentSupport.createXmlElement(doc, XML_ELEMENT_CLASS, className, testNameElement);
            }
        }
    }
}
