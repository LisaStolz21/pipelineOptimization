package de.ee.selectiveexec.control;

import de.ee.optimization.EnvironmentConfiguration;
import de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;

/**
 * Test Class
 */
public class GitDiffReader {

    public static final String BRANCH_PREFIX = "origin/";

    public static void main(final String[] args) throws IOException, InterruptedException {
        final String systemPropRepoUrl = EnvironmentConfiguration.getSystemProperty("remote.repository.url");
        final String testRepo = args[1];

        final String repoUrl = systemPropRepoUrl != null ? systemPropRepoUrl : testRepo;
        System.out.println("property: " + systemPropRepoUrl);
        System.out.println("argument: " + testRepo);
        final String branchName = getFullBranchName(args);
        final List<String> changedJavaClasses = RemoteChangesProcessor.getChangedJavaClasses(branchName, repoUrl);
        changedJavaClasses.forEach(System.out::println);
        final String propertiesFilePath = SelectiveExecutionPropertiesProvider.getSelectiveExecPropertiesPath();
        RelevantTestsCalculator.determineRelevantTests(changedJavaClasses, propertiesFilePath);
    }

    public static String getFullBranchName(final String[] args) {
        final Optional<String> argument = Arrays.stream(args).findFirst();
        return argument.map(s -> BRANCH_PREFIX + s).orElse(null);
    }
}

