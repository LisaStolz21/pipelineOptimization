package de.ee.selectiveexec.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.traversal.DocumentTraversal;
import org.w3c.dom.traversal.NodeFilter;
import org.w3c.dom.traversal.NodeIterator;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class XmlDocumentSupport {
    private static final String TAG_ROOT_TEST_CLASSES_MAPPING = "TestNameCoveredClassesMapping";
    private static final String TAG_TEST_NAME = "TestName";
    private static final String TAG_CLASS = "Class";

    private static final Logger logger = LogManager.getLogger(XmlDocumentSupport.class);

    public static Element createXmlChildOfRootElement(final Document doc, final String elementName, final String elementText) {
        final Element element = doc.createElement(elementName);
        element.appendChild(doc.createTextNode(elementText));
        final Element root = doc.getDocumentElement();
        root.appendChild(element);
        return element;
    }

    public static void createXmlElement(final Document doc, final String elementName, final String elementText, final Element parentElement) {
        final Element element = doc.createElement(elementName);
        element.appendChild(doc.createTextNode(elementText));
        parentElement.appendChild(element);
    }

    public static void createXmlRootElement(final Document doc, final String elementName) {
        final Element rootElement = doc.createElement(elementName);
        doc.appendChild(rootElement);
    }

    public static Transformer getTransformer() throws TransformerConfigurationException {
        final Transformer transformer = TransformerFactory.newInstance().newTransformer();
        transformer.setOutputProperty(OutputKeys.INDENT, "no");
        transformer.setOutputProperty(OutputKeys.METHOD, "xml");
        return transformer;
    }

    public static DocumentBuilder getDocumentBuilder() throws ParserConfigurationException {
        final DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
        return docFactory.newDocumentBuilder();
    }

    public static void saveXmlFile(final Document doc, final String pathToFile) {
        try {
            final DOMSource source = new DOMSource(doc);
            final StreamResult result = new StreamResult(new File(pathToFile));
            XmlDocumentSupport.getTransformer().transform(source, result);
            logger.info("xml file successfully saved! " + pathToFile);
        } catch (final TransformerException e) {
            logger.error("error occurred while processing xml file " + pathToFile);
        }
    }

    public static Document getXmlDocumentByPath(final String pathToXml) {
        final Document document;
        try {
            document = XmlDocumentSupport.getDocumentBuilder().parse(pathToXml);
            return document;
        } catch (final ParserConfigurationException | IOException | SAXException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static boolean isElementAlreadyPresent(final Document doc, final String text, final String tagName) {
        return findNode(doc, text, tagName) != null;
    }

    public static Node findNode(final Document doc, final String text, final String tagName) {
        return getAllNodesByTagName(doc, tagName).stream().filter(nd -> nd.getTextContent().equals(text)).findFirst().orElse(null);
    }

    public static void removeNode(final Document doc, final Node node) {
        doc.getDocumentElement().removeChild(node);
    }

    public static void removeAllNodesByTagName(final Document doc, final String tagName) {
        getAllNodesByTagName(doc, tagName).forEach(node -> removeNode(doc, node));
    }

    public static List<Node> getAllNodesByTagName(final Document doc, final String tagName) {
        final List<Node> nodes = new ArrayList<Node>();
        final NodeList nodeList = doc.getElementsByTagName(tagName);
        for (int temp = 0; temp < nodeList.getLength(); temp++) {
            final Node node = nodeList.item(temp);
            if (node.getNodeType() == Node.ELEMENT_NODE) {
                nodes.add(node);
            }
        }
        return nodes;
    }

    public static void removeNodeByValueAndTagName(final Document document, final String value, final String tagName) {
        final DocumentTraversal traversal = (DocumentTraversal) document;
        final NodeIterator iterator = traversal.createNodeIterator(
                document.getDocumentElement(), NodeFilter.SHOW_ELEMENT, null, true);

        for (Node n = iterator.nextNode(); n != null; n = iterator.nextNode()) {
            final Element currentE = ((Element) n);
            if (currentE.getNodeName().equals(tagName)) {
                if (currentE.hasChildNodes()) {
                    final String text = currentE.getFirstChild().getTextContent().replaceAll("\n", "").trim();
                    if (value.equals(text)) {
                        currentE.getParentNode().removeChild(currentE);
                    }
                }
            }
        }
    }
}
