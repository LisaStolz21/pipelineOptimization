package de.ee.selectiveexec.control;

import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Objects;

import static de.ee.selectiveexec.entity.ConstantValues.TEXT_RED;
import static de.ee.selectiveexec.entity.ConstantValues.TEXT_RESET;

/**
 * csv reader processes the information from the jacoco coverage report csv file and transfers it into a map
 */
public class CoverageReportCSVReader {

    private static final String PACKAGE_COL = "PACKAGE";
    private static final String CLASS_COL = "CLASS";
    private static final String INSTRUCTION_COVERED_COL = "INSTRUCTION_COVERED";
    private static final Logger logger = LogManager.getLogger(CoverageReportCSVReader.class);

    /**
     * Creates a Map<String,List<String>> of the corresponding test and the concerned Java classes from the report
     *
     * @param testName  of the latest executed test, which can be specified by the instance of
     *                  {@link de.ee.selectiveexec.boundary.AbstractCoverageListener}
     * @param pathToCsv the path to the coverage report in csv-format
     */
    public Map<String, List<String>> getMapCoveredClassesByTestName(final String pathToCsv, final String testName) {
        try {
            final BufferedReader bufferedReader = new BufferedReader(new FileReader(pathToCsv));
            final Iterable<CSVRecord> records =
                    CSVFormat.EXCEL.withDelimiter(',').withFirstRecordAsHeader().withTrim().parse(bufferedReader);

            final List<String> classesWithCoveredInstructionFromReport = getAllClassesWithCoveredInstructionFromReport(records);
            classesWithCoveredInstructionFromReport.forEach(System.out::println);
            bufferedReader.close();
            return createMapCoveredClassesByTestName(testName, classesWithCoveredInstructionFromReport);
        } catch (final IOException e) {
            logger.error(TEXT_RED + "error occurred while processing csv file " + pathToCsv + TEXT_RESET + e);
            return null;
        }
    }

    /**
     * Creates a List<String> with all Java classes whose code was partial or total covered by the execution of a specific test
     * class is affected if the covered instruction is not 0
     * column INSTRUCTION_COVERED != 0 from jacoco coverage report.csv
     */
    public List<String> getAllClassesWithCoveredInstructionFromReport(final Iterable<CSVRecord> records) {
        final List<String> affectedClassesByCoverage = new ArrayList<String>();
        for (final CSVRecord record : records) {
            if (!Objects.equals(record.get(INSTRUCTION_COVERED_COL), "0")) {
                final String packageString = record.get(PACKAGE_COL) + ".";
                final String javaClassString = record.get(CLASS_COL);
                affectedClassesByCoverage.add(packageString + javaClassString);
            }
        }
        return affectedClassesByCoverage;
    }

    /**
     * Creates a Map with key {Name of the Test} and mapped value {List of java classes whose code covered by the Test}
     * List<String> affectedClassesByCoverage with package and classname for example de.ee.optimization.integrationtest.control.AccountManagementControl
     * String the Name of the specific Test GamesOverviewPageIntegrationTest
     */
    public Map<String, List<String>> createMapCoveredClassesByTestName(final String TestName, final List<String> affectedClassesByCoverage) {
        final Map<String, List<String>> coveredClassesByTestName = new HashMap<String, List<String>>();
        coveredClassesByTestName.put(TestName, affectedClassesByCoverage);
        return coveredClassesByTestName;
    }

}
