package de.ee.selectiveexec.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintStream;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.Objects;
import java.util.concurrent.TimeUnit;

/**
 * The Executor Class provides methods to execute java statement at runtime
 */
public class RuntimeExecutor {

    private static final String TIME_OUT = "TIME OUT: waited 30 seconds for the process to finish ";

    private static final Logger logger = LogManager.getLogger(RuntimeExecutor.class);

    /**
     * Starts a process to execute a Java statement at runtime.
     * The process is constantly checked to determine if the thread has been completely closed.
     *
     * @param statementToExecute the java stmt to execute
     */
    public static void execute(final String statementToExecute) {
        try {
            final Process process = Runtime.getRuntime().exec(statementToExecute);
            waitForProcess(process);
        } catch (final IOException ex) {
            logger.warn("error occurred while executing statement" + statementToExecute + ex);
        }
    }

    /**
     * Uses the Process Builder to execute commands
     *
     * @param command the command to run
     * @return the Process
     */
//    public static Process runCommand(final Path directory, final String... command) {
//        Objects.requireNonNull(directory, "directory");
//        if (!Files.exists(directory)) {
//            throw new RuntimeException("can't run command in non-existing directory '" + directory + "'");
//        }
//        try {
//            final ProcessBuilder pb = new ProcessBuilder().command(command).directory(directory.toFile());
//            return pb.start();
//        } catch (final IOException ex) {
//            throw new RuntimeException("error occurred while executing command: " + command + ex);
//        }
//    }

    /**
     * Uses the Process Builder to execute commands
     *
     * @param command the command to run
     * @return the Process
     */
    public static Process runCommand(final Path directory, final boolean printStream, final String... command) {
        Objects.requireNonNull(directory, "directory");
        if (!Files.exists(directory)) {
            throw new RuntimeException("can't run command in non-existing directory '" + directory + "'");
        }
        try {
            final ProcessBuilder pb = new ProcessBuilder().command(command).directory(directory.toFile());
            final Process p = pb.start();
            if (printStream) {
                readProcessStreams(p.getErrorStream(), System.err);
                readProcessStreams(p.getInputStream(), System.out);
            }
            return p;
        } catch (final IOException ex) {
            throw new RuntimeException("error occurred while executing command: " + command + ex);
        }
    }

    public static Process runNormalCommand(final String... command) {
        try {
            final ProcessBuilder pb = new ProcessBuilder().command(command);
            return pb.start();
        } catch (final IOException ex) {
            throw new RuntimeException("error occurred while executing command: " + command + ex);
        }
    }

    /**
     * Starts a process to execute statement or command at runtime.
     * The process is constantly checked to determine if the thread has been completely closed.
     * after 30 Seconds Timeout the process will be destroyed
     *
     * @param process the process to watch for
     */
    public static void waitForProcess(final Process process) {
        try {
            if (!process.waitFor(30, TimeUnit.SECONDS)) {
                logger.warn(TIME_OUT);
                process.destroy();
                logger.warn("process destroyed");
            }
        } catch (final InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    private static void readProcessStreams(final InputStream src, final PrintStream dest) {
        new Thread(new Runnable() {
            final BufferedReader br = new BufferedReader(new InputStreamReader(src));

            public void run() {
                try {
                    String line;
                    while ((line = br.readLine()) != null) {
                        dest.println(line);
                    }
                } catch (final IOException ioe) {
                    ioe.printStackTrace();
                }
            }
        }).start();
    }

}
