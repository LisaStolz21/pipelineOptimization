package de.ee.selectiveexec.control;

import de.ee.optimization.TimeUtils;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileSupport {

    /**
     * Creates a file with the specified path.
     *
     * @param path the path to the file
     * @return the File
     */
    public static File getFileByPath(final String path) {
        return new File(path);
    }

    /**
     * Creates a file with the specified path. The file is returned only if it exists and does not represent a directory.
     *
     * @param path the path to the file
     * @return the File
     */
    public static File getFileIfExists(final String path) {
        final File file = new File(path);
        if (file.exists() && !file.isDirectory()) {
            return file;
        } else {
            return null;
        }
    }

    /**
     * Deletes a file if it can be found by the given path
     *
     * @param path the path to the file to delete
     */
    public static void deleteFileIfExists(final String path) {
        try {
            Files.deleteIfExists(Paths.get(path));
            System.out.println("File successfully deleted " + path);

        } catch (final IOException e) {
            e.printStackTrace();
        }
    }

    /**
     * Creates a File by the given path and checks it exists. If the expected file does not exist yet,
     * it waits continuously for one second and checks for it again.
     * The wait is terminated after a maximum of 20 seconds.
     *
     * @param path the path to the file
     */
    public static void waitUntilFileExists(final String path) {
        System.out.println("wait for file " + path);
        final File recentlyCreatedFile = new File(path);
        int maximumSeconds = 0;
        while (!recentlyCreatedFile.exists()) {
            TimeUtils.waitSeconds(1);
            maximumSeconds++;
            System.out.println("checking for file since " + maximumSeconds + " seconds");
            if (maximumSeconds == 20) {
                System.out.println("waited 20 seconds! Can't find File " + path);
                break;
            }
        }
        TimeUtils.waitSeconds(5);
    }

    public static void checkDirectoryExists(final String path) {
        final File directory = new File(path);
        if (directory.exists()) {
            System.out.println("directory exists " + directory);
        }
        if (directory.isDirectory()) {
            System.out.println("it is a directory " + directory);

        }
        final File[] filesList = directory.listFiles();
        for (final File file : filesList) {
            System.out.println("path of file: " + file.getAbsolutePath());

        }
    }
}
