package de.ee.selectiveexec.control;

import de.ee.selectiveexec.entity.OptionPair;
import de.ee.selectiveexec.entity.TestLevel;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedBuilderParametersImpl;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.ex.ConfigurationException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.w3c.dom.Document;

import java.io.File;
import java.util.ArrayList;
import java.util.EnumSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.StringJoiner;

import static de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider.getDefaultTestExecutionList;
import static de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider.getPathToFile;
import static de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider.getTestExecutionList;

/**
 * This class determines the tests to be executed for the test reduction in the pipeline
 * by comparing the changes in the current branch and the test coverage per test.
 * <p>
 * there should be a xml file with multiple mappings between each test and a list of java classes affected by the
 * respective test coverage. The path to the existing xml file is shown in the selective-execution.properties file
 * there are different configurations for each testType {@link de.ee.selectiveexec.entity.TestLevel}
 * <p>
 */
public class RelevantTestsCalculator {
    private static final Logger logger = LogManager.getLogger(RelevantTestsCalculator.class);
    private static final EnumSet<TestLevel> integrationTestLevel = EnumSet.of(TestLevel.UI_TEST, TestLevel.API_TEST);

    public static void determineRelevantTests(final List<String> changedClasses, final String propertiesFilePath) {
        final List<OptionPair> executionOptions = calculateTestExecutionOptions(changedClasses);
        updateSelectiveExecutionProperties(executionOptions, propertiesFilePath);
    }

    private static List<OptionPair> calculateTestExecutionOptions(final List<String> gitChangeList) {
        final List<OptionPair> configurations = new ArrayList<OptionPair>();
        for (final TestLevel testLevel : integrationTestLevel) {

            final String testsToExecute = getRelevantTestsAsString(testLevel, gitChangeList);
            final String executionListPropertyKey;
            final String executionListPropertyValue;
            if (testsToExecute == null) {
                executionListPropertyKey = getDefaultTestExecutionList(testLevel, true);
                executionListPropertyValue = getDefaultTestExecutionList(testLevel, false);
            } else {
                executionListPropertyKey = getTestExecutionList(testLevel, true);
                executionListPropertyValue = getTestExecutionList(testLevel, false);
            }
            configurations.add(new OptionPair(executionListPropertyKey, executionListPropertyValue));
        }
        return configurations;
    }

    private static String getRelevantTestsAsString(final TestLevel level, final List<String> gitChangeList) {
        final Map<String, Set<String>> coverageResultMapping = findExistingCoverageMap(level);
        if (coverageResultMapping == null) {
            return null;
        }

        final StringJoiner joiner = new StringJoiner(", ");
        for (final String key : coverageResultMapping.keySet()) {
            final boolean isTestExecutionNecessary = isTestRelevant(gitChangeList, coverageResultMapping.get(key));
            if (isTestExecutionNecessary) {
                joiner.add(key);
            }
        }
        return joiner.toString();
    }

    private static boolean isTestRelevant(final List<String> gitDiffs, final Set<String> coverageAffectedClasses) {
        return coverageAffectedClasses.stream().anyMatch(gitDiffs::contains);
    }

    private static Map<String, Set<String>> findExistingCoverageMap(final TestLevel level) {
        final Document document = XmlDocumentSupport.getXmlDocumentByPath(getPathToFile(level));
        if (document == null) {
            logger.warn("could not find existing file for coverage data, all tests of Type " + level.name() + " will be executed");
            return null;
        }
        return CoverageAnalysisResultsXmlReader.createMappingFromXml(document);
    }

    private static void updateSelectiveExecutionProperties(final List<OptionPair> executionOptions, final String propertiesFilePath) {
        final FileBasedConfigurationBuilder<PropertiesConfiguration> builder = getConfigurationBuilder(propertiesFilePath);
        try {
            final PropertiesConfiguration config = builder.getConfiguration();
            for (final OptionPair optionPair : executionOptions) {
                config.setProperty(optionPair.getOption(), optionPair.getValue());
            }
            builder.resetResult();
            builder.save();
        } catch (final ConfigurationException io) {
            io.printStackTrace();
        }
    }

    private static FileBasedConfigurationBuilder<PropertiesConfiguration> getConfigurationBuilder(final String propertiesFilePath) {
        final File file = new File(propertiesFilePath);
        return new FileBasedConfigurationBuilder<PropertiesConfiguration>(PropertiesConfiguration.class)
                .configure(new FileBasedBuilderParametersImpl().setFile(file));
    }
}
