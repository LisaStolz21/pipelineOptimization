package de.ee.selectiveexec.control;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class CoverageAnalysisResultsXmlReader {

    private static final String XML_ELEMENT_TEST_NAME = "TestName";
    private static final String XML_ELEMENT_CLASS = "Class";

    public static Map<String, Set<String>> createMappingFromXml(final Document document) {

        final Map<String, Set<String>> coveredClassesByTestName = new HashMap<String, Set<String>>();
        document.getDocumentElement().normalize();
        final NodeList testNames = document.getElementsByTagName(XML_ELEMENT_TEST_NAME);

        for (int temp = 0; temp < testNames.getLength(); temp++) {
            final Node nodeTestName = testNames.item(temp);
            if (nodeTestName.getNodeType() == Node.ELEMENT_NODE) {
                final Element element = (Element) nodeTestName;
                final String testName = element.getAttribute("testName");
                coveredClassesByTestName.put(testName, getCoveredAffectedClasses((Element) nodeTestName));
            }
        }
        return coveredClassesByTestName;
    }

    public static Set<String> getCoveredAffectedClasses(final Element testName) {
        final Set<String> affectedClasses = new HashSet<String>();
        final NodeList coveredClasses = testName.getElementsByTagName(XML_ELEMENT_CLASS);
        for (int counter = 0; counter < coveredClasses.getLength(); counter++) {
            final Node nodeClass = coveredClasses.item(counter);
            final String className = nodeClass.getTextContent();
            affectedClasses.add(className);
        }
        return affectedClasses;
    }

}
