package de.ee.selectiveexec.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.InputStream;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Arrays;

import static de.ee.selectiveexec.entity.ConstantValues.TEXT_PURPLE;
import static de.ee.selectiveexec.entity.ConstantValues.TEXT_RESET;

/**
 * @author Lisa Stolz
 */
public class VersionControlSynchronizer {
    private static final Logger logger = LogManager.getLogger(VersionControlSynchronizer.class);
    private static boolean isLoggingActive = false;
    private static boolean trackSubProcess = false;
    private static String branchToSync = null;
    private static Path repositoryPath = null;

    private VersionControlSynchronizer(final String branchName, final String repositoryUrl, final boolean logging, final boolean trackProcess) {
        isLoggingActive = logging;
        branchToSync = branchName;
        trackSubProcess = trackProcess;
        repositoryPath = resolvePath(repositoryUrl);
    }

    public static VersionControlSynchronizer initializeSynchronizer(final String branchName, final String repositoryUrl, final boolean logging, final boolean trackProcess) {
        return new VersionControlSynchronizer(branchName, repositoryUrl, logging, trackProcess);
    }

    private VersionControlSynchronizer(final String branchName, final String repositoryUrl) {
        branchToSync = branchName;
        repositoryPath = resolvePath(repositoryUrl);
    }

    public static VersionControlSynchronizer initializeSynchronizer(final String branchName, final String repositoryUrl) {
        return new VersionControlSynchronizer(branchName, repositoryUrl);
    }

    private void gitCloneRepository(final Path targetDirectory, final String remoteUrl) {
        executeAndLog(repositoryPath.getParent(), "git", "clone", remoteUrl, targetDirectory.getFileName().toString());

    }

    public void gitCheckoutAndRebase() {
        gitFetch();
        gitCheckoutBranchByName();
        gitMergeRemote();
        gitStatus();
    }

    public void gitInitialize() {
        executeAndLog(repositoryPath, "git", "init");
    }

    public void gitStatus() {
        executeAndLog(repositoryPath, "git", "status");
    }

    public void gitFetch() {
        executeAndLog(repositoryPath, "git", "fetch");
    }

    public InputStream gitDiff() {
        final String diffString = "origin/main.." + branchToSync;
        final Process p = RuntimeExecutor.runCommand(repositoryPath, trackSubProcess, "git", "diff", diffString);
        return p.getInputStream();
    }

    public void gitPull() {
        executeAndLog(repositoryPath, "git", "pull");
    }

    public void gitCheckoutBranchByName() {
        executeAndLog(repositoryPath, "git", "checkout", branchToSync);
    }

    public void gitPullRebaseBranch() {
        executeAndLog(repositoryPath, "git", "pull", "--rebase", "origin", branchToSync);
    }

    public void gitMergeRemote() {
        final String originBranchName = "origin/" + branchToSync;
        executeAndLog(repositoryPath, "git", "merge", originBranchName);
    }

    public void gitCommit() {
        executeAndLog(repositoryPath, "git", "commit", "-m", "commitment by SYSTEM! ");

    }

    public void gitPush(final boolean skipPipeline) {
        if (skipPipeline) {
            executeAndLog(repositoryPath.getParent(), "git", "push", "-o", "ci.skip");
        } else {
            executeAndLog(repositoryPath.getParent(), "git", "push");
        }
    }

    public void gitAddFile(final String pathToSaveFile, final String fileName) {
        final Path targetDirectory = repositoryPath.resolve(pathToSaveFile);
        executeAndLog(targetDirectory, "git", "add", fileName);
    }

    private void executeAndLog(final Path directory, final String... command) {
        RuntimeExecutor.runCommand(directory, trackSubProcess, command);
        if (isLoggingActive) {
            logger.info(TEXT_PURPLE + "executing GIT COMMAND ------- " + Arrays.toString(command) + " -------" + TEXT_RESET);
        }
    }

    public static Path resolvePath(final String pathToRepository) {
        return Paths.get(pathToRepository);
    }

    public Path getRepositoryPath() {
        return repositoryPath;
    }
}
