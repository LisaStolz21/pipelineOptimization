package de.ee.selectiveexec.control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.file.Path;

import static de.ee.selectiveexec.entity.ConstantValues.TEXT_PURPLE;
import static de.ee.selectiveexec.entity.ConstantValues.TEXT_RESET;

/**
 * @author Lisa Stolz
 */
public class VersionControlManager {
    private static final Logger logger = LogManager.getLogger(VersionControlManager.class);
    private static final boolean isLoggingActive = true;

    public static void gitCloneRepository(final Path targetDirectory, final String remoteUrl) {
        runCommand(targetDirectory.getParent(), "git", "clone", remoteUrl, targetDirectory.getFileName().toString());
    }

    public static void gitCheckoutBranch(final Path targetDirectory, final String branchName) {
        gitFetch(targetDirectory);
        gitCheckoutBranchByName(targetDirectory, branchName);
        gitStatus(targetDirectory);
        gitPull(targetDirectory);
        gitStatus(targetDirectory);

//        gitInitialize(targetDirectory);

    }

//    public static void gitCheckoutAndUpdateBranch(final Path targetDirectory, final String branchName) {
//        gitCheckoutBranchByName(targetDirectory, branchName);
//        gitStatus(targetDirectory);
//        gitInitialize(targetDirectory);
//        gitFetch(targetDirectory);
//        gitPullRebaseBranch(branchName);
//    }

    public static void gitInitialize(final Path directoryPath) {
        loggGitCommand("init", isLoggingActive);
        runCommand(directoryPath, "git", "init");
    }

    public static void gitStatus(final Path directoryPath) {
        loggGitCommand("status", isLoggingActive);
        runCommand(directoryPath, "git", "status");
    }

    public static void gitFetch(final Path directoryPath) {
        loggGitCommand("fetch", isLoggingActive);
        runCommand(directoryPath, "git", "fetch");
    }

    public static void gitPull(final Path directoryPath) {
        loggGitCommand("pull", isLoggingActive);
        runCommand(directoryPath, "git", "pull");
    }

    public static void gitCheckoutBranchByName(final Path directoryPath, final String branchName) {
        loggGitCommand("checkout", isLoggingActive);
        runCommand(directoryPath, "git", "checkout", branchName);
    }

    public static void gitPullRebaseBranch(final String branchName) {
        loggGitCommand("pull -- rebase (normal command)", isLoggingActive);
        RuntimeExecutor.runNormalCommand("git", "pull", "--rebase", "origin", branchName);
    }

    public static void gitCommit(final Path directoryPath) {
        loggGitCommand("commit", isLoggingActive);
        runCommand(directoryPath.getParent(), "git", "commit", "-m", "commitment by SYSTEM! ");
    }

    public static void gitPush(final Path directoryPath) {
        loggGitCommand("push", isLoggingActive);
        runCommand(directoryPath.getParent(), "git", "push", "-o", "ci.skip");
    }

    public static void gitAdd(final Path directoryPath, final String fileName) {
        loggGitCommand("add", isLoggingActive);
        runCommand(directoryPath, "git", "add", fileName);
    }

    public static void runCommand(final Path directory, final String... command) {
        printStream(RuntimeExecutor.runCommand(directory, true, command).getInputStream());
        printStream(RuntimeExecutor.runCommand(directory, true, command).getErrorStream());
    }

    public static void printStream(final InputStream inputStream) {
        final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                System.out.println(line);
            }
        } catch (final IOException ioe) {
            ioe.printStackTrace();
        }
    }

    private static void loggGitCommand(final String command, final boolean isLoggingActive) {
        if (isLoggingActive) {
            logger.info(TEXT_PURPLE + "executing GIT COMMAND ------- " + command + " -------" + TEXT_RESET);
        }
    }
}
