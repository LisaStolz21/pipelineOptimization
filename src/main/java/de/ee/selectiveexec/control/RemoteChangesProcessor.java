package de.ee.selectiveexec.control;

import de.ee.selectiveexec.entity.GitFileMode;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * The processor is responsible for detecting the code changes within a feature branch.
 * The basis for comparison is currently the master branch
 */
public class RemoteChangesProcessor {

    private static final String CLASS_FILE_IDENTIFIER = "src/main/java";
    private static final String NEW_FILE_MODE = "new file mode";
    private static final String DELETED_FILE_MODE = "deleted file mode";
    private static final Logger logger = LogManager.getLogger(RemoteChangesProcessor.class);

    public static List<String> getChangedJavaClasses(final String featureBranchName, final String repositoryUrl) {
        if (featureBranchName == null) {
            logger.error("could not identify Gitlab branch name ");
            return Collections.emptyList();
        }

        final VersionControlSynchronizer vcs = VersionControlSynchronizer.initializeSynchronizer(featureBranchName, repositoryUrl);
        return getChangedJavaClasses(vcs.gitDiff());
    }

    public static List<String> getChangedJavaClasses(final InputStream inputStream) {
        final List<String> allChangedFilesAndClasses = new ArrayList<String>();

        final BufferedReader br = new BufferedReader(new InputStreamReader(inputStream));
        try {
            String line;
            while ((line = br.readLine()) != null) {
                if (line.contains("diff --git") && line.contains(CLASS_FILE_IDENTIFIER)) {
                    allChangedFilesAndClasses.add(getPathToJavaClass(line));
                }
            }
        } catch (final IOException ioe) {
            ioe.printStackTrace();
        }
        return allChangedFilesAndClasses;
    }

    /**
     * saves all classes for which code coverage is evident after execution of the test to a
     * specific xml file for either the results for Selenium or Cucumber tests.
     * The affected classes are determined by extracting information from the csv reports
     * {@link CoverageReportCSVReader}
     */
    private static String getPathToJavaClass(final String diffLine) {
        String pathToClass = "";
        try {
            String line = diffLine;
            line = line.substring(line.indexOf("a/") + 1);
            line = line.substring(0, line.indexOf("b/"));
            line = line.replaceAll("/", ".");
            pathToClass = line.replace(".src.main.java.", "");

        } catch (final StringIndexOutOfBoundsException e) {
            logger.warn("index out of bounds ---> " + diffLine);
        }
        return pathToClass;
    }

    private static List<String> fileToLines(final String pathToFile) {
        final List<String> lines = new LinkedList<>();
        String line;
        try {
            final File file = new File(pathToFile);
            final BufferedReader input = new BufferedReader(new FileReader(file));
            while ((line = input.readLine()) != null) {
                lines.add(line);
            }
        } catch (final IOException e) {
            e.printStackTrace();
        }

        return lines;
    }

    /**
     * detects the file mode and parses it to {@link GitFileMode}
     *
     * @param line the current line
     * @return the file mode
     */
    public static GitFileMode getFileModeOfLine(final String line) {
        if (line.contains(NEW_FILE_MODE)) {
            return GitFileMode.NEW;
        } else if (line.contains(DELETED_FILE_MODE)) {
            return GitFileMode.DELETED;
        } else {
            return GitFileMode.CHANGED;
        }
    }

    /**
     * is collecting the List of changed Java Classes through reading a patch file
     *
     * @param path the path to the patch file
     * @return List<String> of changed classes
     */
    public static List<String> getAllChangedFilesGitDiff(final String path) {

        final List<String> allChangedFilesAndClasses = new ArrayList<String>();
        final List<String> allLines = fileToLines(path);

        for (final String line : allLines) {
            if (line.contains("diff --git") && line.contains(CLASS_FILE_IDENTIFIER)) {
                allChangedFilesAndClasses.add(getPathToJavaClass(line));
            }
        }
        return allChangedFilesAndClasses;
    }

}
