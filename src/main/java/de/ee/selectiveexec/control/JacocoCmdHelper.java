package de.ee.selectiveexec.control;

import de.ee.selectiveexec.boundary.CoverageCommandLineOptionsProvider;
import de.ee.selectiveexec.entity.CoverageCommandLineOptions;
import de.ee.selectiveexec.entity.JacocoCoverageGoal;
import de.ee.selectiveexec.entity.TestLevel;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.List;
import java.util.Map;

import static de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider.getPathToFile;

/**
 * jacoco per test coverage implementation with usage of the jacoco Command Line Interface.
 * The Generation of the coverage report requires a view options to be set for each goal of the jacoco-maven-plugin
 * The main goals for generating integration test coverage report are to prepare the agent, dump the data, and create the report
 * Since the coverage is measured per test, the executable must be reset after each test run.
 * <p>
 * this class is responsible for implement the goals: {@link de.ee.selectiveexec.entity.JacocoCoverageGoal}
 */
public class JacocoCmdHelper {

    private static final String commandJacocoCliJar = "java -jar src/main/resources/coverage-reports/org.jacoco.cli-0.8.5-nodeps.jar ";
    private static final String commandJacocoAgentJar = "java -javaagent:src/main/resources/coverage-reports/org.jacoco.agent-0.8.5-runtime.jar=";
    private static final Logger logger = LogManager.getLogger(JacocoCmdHelper.class);

    /**
     * dump data after test execution
     */
    public static void dump(final CoverageCommandLineOptions commandLineOptions) {
        executeStatementByGoal(JacocoCoverageGoal.DUMP, commandLineOptions);
    }

    /**
     * generates a coverage report in csv format containing the information of the dumped data
     */
    public static void report(final CoverageCommandLineOptions commandLineOptions) {
        executeStatementByGoal(JacocoCoverageGoal.REPORT, commandLineOptions);
    }

    /**
     * saves all classes for which code coverage is evident after execution of the test to a
     * specific xml file for either the results for Selenium or Cucumber tests.
     * The affected classes are determined by extracting information from the csv reports
     * {@link CoverageReportCSVReader}
     */
    public static void store(final String testName, final CoverageCommandLineOptions commandLineOptions, final TestLevel testLevel) {
        storeCoverageDataToXml(testName, commandLineOptions, testLevel);
    }

    /**
     * resets the jacoco executable before running a new test
     */
    public static void reset(final CoverageCommandLineOptions commandLineOptions) {
        executeStatementByGoal(JacocoCoverageGoal.RESET, commandLineOptions);
    }

    /**
     * adds jacoco agent to jvm
     */
    public static void prepare() {
        prepareJacocoAgent();
    }

    /**
     * deletes the csv report after the retrieved  information has been saved
     */
    public static void deleteReport(final CoverageCommandLineOptions commandLineOptions) {
        deleteCsv(commandLineOptions);
    }

    private static void storeCoverageDataToXml(final String testName, final CoverageCommandLineOptions
            commandLineOptions, final TestLevel testLevel) {

        final String pathToCsv = commandLineOptions.getCsv().getValue().replaceAll("--csv ", "").trim();
        final CoverageReportCSVReader csvReader = new CoverageReportCSVReader();
        final Map<String, List<String>> testNameCoveredClassesMap = csvReader.getMapCoveredClassesByTestName(pathToCsv, testName);
        final String pathToSaveMappingXml = getPathToFile(testLevel);
        final CoverageAnalysisResultsXmlWriter xmlWriter = new CoverageAnalysisResultsXmlWriter();
        xmlWriter.createOrUpdateXmlByCoverageReportMap(testNameCoveredClassesMap, pathToSaveMappingXml);
    }

    private static void deleteCsv(final CoverageCommandLineOptions commandLineOptions) {
        final String pathToCsv = commandLineOptions.getCsv().getValue().replaceAll("--csv ", "").trim();
        FileSupport.deleteFileIfExists(pathToCsv);
    }

    private static void executeStatementByGoal(final JacocoCoverageGoal goal, final CoverageCommandLineOptions commandLineOptions) {
        final String destFile = commandLineOptions.getDestFile().getValue().replaceAll("--destfile ", "").trim();
        switch (goal) {
            case PREPARE:
                return;
            case DUMP:
                final String dumpStatement = commandJacocoCliJar + goal.getName() +
                        commandLineOptions.getAddress().getValue() + commandLineOptions.getPort().getValue() +
                        commandLineOptions.getDestFile().getValue();
                RuntimeExecutor.execute(dumpStatement);
                return;
            case REPORT:
                final String reportStatement = commandJacocoCliJar + goal.getName() + " " + destFile +
                        commandLineOptions.getClassFiles().getValue() + commandLineOptions.getSourceFiles().getValue() +
                        commandLineOptions.getCsv().getValue() + commandLineOptions.getHtml().getValue() +
                        commandLineOptions.getName().getValue();
                RuntimeExecutor.execute(reportStatement);
                return;
            case RESET:
                final String resetStatement = commandJacocoCliJar + JacocoCoverageGoal.DUMP.getName() +
                        commandLineOptions.getDestFile().getValue() + commandLineOptions.getAddress().getValue() +
                        commandLineOptions.getPort().getValue() + commandLineOptions.getReset();
                RuntimeExecutor.execute(resetStatement);
                return;
            default:
                throw new IllegalStateException("no valid type of JacocoCoverageGoal: " + goal);
        }
    }

    private static void prepareJacocoAgent() {
        final List<String> commandLineOptions = CoverageCommandLineOptionsProvider.getOptionsPrepare();
        final String prepareStmt = commandJacocoAgentJar + String.join(",", commandLineOptions);
        RuntimeExecutor.execute(prepareStmt);
    }

}
