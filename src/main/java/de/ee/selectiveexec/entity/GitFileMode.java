package de.ee.selectiveexec.entity;

/**
 * The type of git file mode
 */
public enum GitFileMode {
    DELETED("deleted"),
    NEW("new"),
    CHANGED(null);

    private final String fileModeName;


    GitFileMode(final String fileModeName) {
        this.fileModeName = fileModeName;

    }

    public String getName() {
        return fileModeName;
    }
}


