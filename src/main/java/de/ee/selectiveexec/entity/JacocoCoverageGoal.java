package de.ee.selectiveexec.entity;

/**
 * The important goals for generating per test coverage reports
 */
public enum JacocoCoverageGoal {
    PREPARE,
    DUMP,
    REPORT,
    RESET;

    public String getName() {
        return name().toLowerCase();
    }
}
