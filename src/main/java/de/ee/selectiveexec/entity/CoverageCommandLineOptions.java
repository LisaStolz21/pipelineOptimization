package de.ee.selectiveexec.entity;

public class CoverageCommandLineOptions {

    private OptionPair destFile;
    private OptionPair address;
    private OptionPair port;
    private OptionPair classFiles;
    private OptionPair sourceFiles;
    private OptionPair output;
    private OptionPair append;

    private OptionPair html;
    private OptionPair csv;
    private OptionPair name;

    public OptionPair getDestFile() {
        return destFile;
    }

    public OptionPair getAddress() {
        return address;
    }

    public OptionPair getPort() {
        return port;
    }

    public OptionPair getClassFiles() {
        return classFiles;
    }

    public OptionPair getSourceFiles() {
        return sourceFiles;
    }

    public OptionPair getOutput() {
        return output;
    }

    public OptionPair getAppend() {
        return append;
    }

    public OptionPair getHtml() {
        return html;
    }

    public OptionPair getCsv() {
        return csv;
    }

    public OptionPair getName() {
        return name;
    }

    public void setAppend(final OptionPair append) {
        this.append = append;
    }

    public void setHtml(final OptionPair html) {
        this.html = html;
    }

    public void setCsv(final OptionPair csv) {
        this.csv = csv;
    }

    public void setName(final OptionPair name) {
        this.name = name;
    }

    public void setDestFile(final OptionPair destFile) {
        this.destFile = destFile;
    }

    public void setAddress(final OptionPair address) {
        this.address = address;
    }

    public void setPort(final OptionPair port) {
        this.port = port;
    }

    public void setClassFiles(final OptionPair classFiles) {
        this.classFiles = classFiles;
    }

    public void setSourceFiles(final OptionPair sourceFiles) {
        this.sourceFiles = sourceFiles;
    }

    public void setOutput(final OptionPair output) {
        this.output = output;
    }

    public String getReset() {
        return " --reset";
    }

}
