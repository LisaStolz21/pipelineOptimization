package de.ee.selectiveexec.entity;

public enum JacocoCoverageOutputType {
    FILE("file"),
    TCP_SERVER("tcpserver"),
    TCP_CLIENT("tcpclient"),
    NONE("none");

    private final String outputType;

    JacocoCoverageOutputType(final String outputType) {
        this.outputType = outputType;

    }

    public String getName() {
        return outputType;
    }
}
