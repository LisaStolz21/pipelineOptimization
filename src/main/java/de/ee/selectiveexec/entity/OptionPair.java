package de.ee.selectiveexec.entity;

public class OptionPair {

    private final String option;
    private final String value;

    public OptionPair(final String option, final String value) {
        this.option = option;
        this.value = value;
    }

    /**
     * Gets the option.
     *
     * @return value of option
     */
    public String getOption() {
        return option;
    }

    /**
     * Gets the value.
     *
     * @return value of value
     */
    public String getValue() {
        return value;
    }
}
