package de.ee.selectiveexec.entity;

public class ConstantValues {

    public static final String TEXT_RESET = "\u001B[0m";
    public static final String TEXT_RED = "\u001B[31m";
    public static final String TEXT_BOLD = "\u001B[1m";
    public static final String TEXT_GREEN = "\u001B[32m";
    public static final String TEXT_YELLOW = "\u001B[33m";
    public static final String TEXT_BLUE = "\u001B[34m";
    public static final String TEXT_PURPLE = "\u001B[35m";
    public static final String TEXT_CYAN = "\u001B[36m";
    public static final String TEXT_WHITE = "\u001B[37m";

    private static final String destFileOption = " --destfile ";
    private static final String addressOption = " --address ";
    private static final String portOption = " --port ";
    private static final String resetOption = " --reset ";
    private static final String htmlOption = " --html";
    private static final String csvOption = " --csv ";
    private static final String classFilesOption = " --classfiles ";

}
