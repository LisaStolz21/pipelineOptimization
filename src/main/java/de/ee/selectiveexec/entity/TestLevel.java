package de.ee.selectiveexec.entity;

public enum TestLevel {

    UNIT_TEST("unit"),
    API_TEST("api"),
    UI_TEST("web"),
    REGRESSION_TEST("regression"),
    ;
    
    private final String key;

    TestLevel(final String key) {
        this.key = key;
    }

    public String getKey() {
        return key;
    }

    public String getName() {
        return name();
    }
}
