package de.ee.selectiveexec.entity;

public class RunningTestParameter {

    private final String testName;
    private final String methodName;
    private final TestLevel testLevel;
    private final int executionTime;

    public RunningTestParameter(final String testName, final String methodName, final TestLevel testLevel, final int executionTime) {
        this.testName = testName;
        this.methodName = methodName;
        this.testLevel = testLevel;
        this.executionTime = executionTime;
    }

    /**
     * Gets the testName.
     *
     * @return value of testName
     */
    public String getTestName() {
        return testName;
    }

    /**
     * Gets the methodName.
     *
     * @return value of methodName
     */
    public String getMethodName() {
        return methodName;
    }

    /**
     * Gets the testLevel.
     *
     * @return value of testLevel
     */
    public TestLevel getTestLevel() {
        return testLevel;
    }

    /**
     * Gets the executionTime.
     *
     * @return value of executionTime
     */
    public int getExecutionTime() {
        return executionTime;
    }
}
