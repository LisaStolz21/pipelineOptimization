INSERT INTO game
VALUES ('1', 'World Of Warcraft', 'RPG', '2004-11-23', 'PC', 'Blizzard Entertainment', null);

INSERT INTO game
VALUES ('2', 'Counter-Strike', 'ACTION', '2000-11-08', 'PC', 'Valve', 'GoldSrc');

INSERT INTO game
VALUES ('3', 'Minecraft', 'SANDBOX', '2009-05-17', 'PC_PS_XBOX_NS', 'Mojang Studios', 'RenderDragon');

INSERT INTO game
VALUES ('4', 'Leage of Legends', 'STRATEGY', '2009-10-27', 'PC', 'Riot Games', null);

INSERT INTO game
VALUES ('5', 'Grand Theft Auto V', 'ACTION', '2013-09-17', 'PC_PS_XBOX', 'Rockstar Games', 'RAGE');

INSERT INTO game
VALUES ('6', 'Call of Duty', 'ACTION', '2003-10-29', 'PC_PS_XBOX', 'Infinity Ward', 'id Tech 3');

INSERT INTO game
VALUES ('7', 'FIFA', 'SIMULATION', '1994-01-01', 'PC_PS_XBOX_NS', 'EA Canada', 'Frostbite-Engine');

INSERT INTO game
VALUES ('8', 'Fallout 4', 'RPG', '2015-11-10', 'PC_PS_XBOX', 'Bethesda', 'Creation Engine');
INSERT INTO game
VALUES ('9', 'Overwatch', 'ACTION', '2016-05-24', 'PC_PS_XBOX_NS', 'Blizzard Entertainment', null);
INSERT INTO game
VALUES ('10', 'The Witcher 3', 'RPG', '2015-05-19', 'PC_PS_XBOX_NS', 'CD Projekt RED', 'REDengine 3');



-- INSERT INTO games
-- VALUES ('3', 'Fortnite', 'RPG', '2022-03-22', 'PC', 'Blizzard Entertainment', null);
-- INSERT INTO games
-- VALUES ('3', 'Final Fantasy', 'RPG', '2022-03-22', 'PC', 'Blizzard Entertainment', null);
-- INSERT INTO games
-- VALUES ('3', 'Age Of Empire', 'RPG', '2022-03-22', 'PC', 'Blizzard Entertainment', null);
--
-- INSERT INTO gamer_account
-- VALUES ('Maximilian', 'Bauer', '2019-05-19', 'max.bauer@web.de');


INSERT INTO wow_expansion
VALUES ('World of Warcraft', 'Retail', 'November 2004');
INSERT INTO wow_expansion
VALUES ('The Burning Crusade', 'Retail', 'January 2007');
INSERT INTO wow_expansion
VALUES ('Wrath of the Lich King', 'Retail', 'November 2008');
INSERT INTO wow_expansion
VALUES ('Cataclysm', 'Retail', 'December 2010');
INSERT INTO wow_expansion
VALUES ('Mists of Pandaria', 'Retail', 'September 2012');
INSERT INTO wow_expansion
VALUES ('Warlords of Draenor', 'Retail', 'November 2014');
INSERT INTO wow_expansion
VALUES ('Legion', 'Retail', 'August 2016');
INSERT INTO wow_expansion
VALUES ('Battle for Azeroth', 'Retail', 'August 2018');
INSERT INTO wow_expansion
VALUES ('World of Warcraft Classic', 'Classic', 'August 2019');
INSERT INTO wow_expansion
VALUES ('Shadowlands', 'Retail', 'November 2020');
INSERT INTO wow_expansion
VALUES ('The Burning Crusade Classic', 'Classic', 'June 2021');
