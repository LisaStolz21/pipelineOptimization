create schema pipelineOpt;
alter role postgres set search_path = pipelineOpt;

create table game
(
    id        varchar(50) NOT NULL,
    name      varchar(50) NOT NULL
        constraint unq_game_name
            unique,
    genre     varchar(25) NOT NULL,
    release   timestamp   NOT NULL,
    platform  varchar(25) NOT NULL,
    developer varchar(25) NOT NULL,
    engine    varchar(25),
    PRIMARY KEY (id)
);

create table platform_type
(
    platform_type_id varchar(25) NOT NULL,
    platform         varchar(25) NOT NULL
);
ALTER TABLE ONLY platform_type
    ADD CONSTRAINT platform_type_pkey PRIMARY KEY (platform_type_id);


create table platform_name
(
    id          varchar(50) NOT NULL,
    name        varchar(25) NOT NULL,
    platform_id varchar(25) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE ONLY platform_name
    ADD CONSTRAINT "FK_platform_id" FOREIGN KEY (platform_id) REFERENCES platform_type (platform_type_id);

create table gamer_account
(
    user_id           varchar(50) NOT NULL,
    first_name        varchar(50) NOT NULL,
    last_name         varchar(50) NOT NULL,
    registration_date timestamp   NOT NULL,
    expiration_date   timestamp   NOT NULL,
    mail_address      varchar(50) NOT NULL,
    PRIMARY KEY (user_id)
);

create table account_registration
(
    id           varchar(50) NOT NULL,
    first_name   varchar(50) NOT NULL,
    last_name    varchar(50) NOT NULL,
    mail_address varchar(50) NOT NULL,
    account_name varchar(50) NOT NULL,
    status       varchar(25) NOT NULL,
    PRIMARY KEY (id)
);

ALTER TABLE ONLY account_registration
    ADD CONSTRAINT registration_mail_pkey PRIMARY KEY (mail_address);

-- CREATE TABLE games_user_mapping
-- (
--     user_id varchar NOT NULL,
--     game_id varchar NOT NULL,
--     CONSTRAINT games_user__fk FOREIGN KEY (user_id) REFERENCES gamer_account (user_id),
--     CONSTRAINT users_game_fk FOREIGN KEY (game_id) REFERENCES game (name)
-- );


create table wow_expansion
(
    expansion      varchar(50) NOT NULL,
    expansion_type varchar(50) NOT NULL,
    release        varchar(50) NOT NULL,
    PRIMARY KEY (expansion)
);