drop table if exists game cascade;

drop table if exists platform_name cascade;

drop table if exists platform_type cascade;

drop table if exists gamer_account cascade;

drop table if exists account_registration cascade;

drop table if exists games_user_mapping cascade;

drop schema if exists pipelineOpt;