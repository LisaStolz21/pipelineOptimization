Feature: Manage new Accounts

  @cleanup
  Scenario: Handling of account management for new customers
    Given a new user wants to create an account registration
    When the request reaches our service the user is verified
    Then the account was successfully created