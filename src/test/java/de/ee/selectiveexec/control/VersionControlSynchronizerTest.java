package de.ee.selectiveexec.control;

import de.ee.optimization.EnvironmentConfiguration;
import de.ee.selectiveexec.boundary.SelectiveExecutionPropertiesProvider;
import de.ee.selectiveexec.entity.TestLevel;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import java.nio.file.Path;
import java.nio.file.Paths;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Lisa Stolz
 */
public class VersionControlSynchronizerTest {
    private static final TestLevel level = TestLevel.API_TEST;
    private static final String propertyRepositoryUrl = "remote.repository.url";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInitializeSynchronizer_testProdRepositoryData() throws Exception {
        System.setProperty(propertyRepositoryUrl, "/home/gitlab-runner/pipelineOptRepo");

        final String nameOfCurrentBranch = "version-control-implementation";
        final String systemPropRepoUrl = EnvironmentConfiguration.getSystemProperty(propertyRepositoryUrl);
        final Path repoPath = Paths.get(systemPropRepoUrl);

        final String pathToFile = SelectiveExecutionPropertiesProvider.getPathToFile(level);
        System.out.println(pathToFile);
        assertThat(pathToFile, is("src/main/resources/coverage-reports/cucumber-result-mapping.xml"));

        final String fileName = SelectiveExecutionPropertiesProvider.getFileName(level);
        assertThat(fileName, is("cucumber-result-mapping.xml"));

        final String saveDirectory = SelectiveExecutionPropertiesProvider.getPath(level);
        assertThat(saveDirectory, is("src/main/resources/coverage-reports"));

        final VersionControlSynchronizer vcs = VersionControlSynchronizer.initializeSynchronizer(nameOfCurrentBranch, systemPropRepoUrl, true, true);
        final Path resultPath = vcs.getRepositoryPath();
        assertThat(resultPath, is(repoPath));

    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme