package de.ee.selectiveexec.control;

import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedBuilderParametersImpl;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;
import testsupport.UnitTestSupport;

import javax.naming.ConfigurationException;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertNotSame;

/**
 * @author Lisa Stolz
 */
public class RelevantTestsCalculatorTest {
    private static final String TEST_EXECUTION_PROPERTIES = "integrationtest-execution-test.properties";

    @Before
    public void setUp() {
        UnitTestSupport.setEntryTestPropertyFile(UnitTestSupport.PROP_UI_RELEVANT_TESTS, "");
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testDetermineRelevantTests_restrictionNotAvailable_determineAndCheckRestrictionEntries() throws Exception {

        final String uiTestsToExecute = UnitTestSupport.findPropertyValue(UnitTestSupport.PROP_UI_RELEVANT_TESTS);
        assertThat(uiTestsToExecute, is(""));

        final String propertiesFilePath = UnitTestSupport.getPathToFile(TEST_EXECUTION_PROPERTIES);
        RelevantTestsCalculator.determineRelevantTests(provideGitDiffList(), propertiesFilePath);

        final String updatedUiTestsToExecute = UnitTestSupport.findPropertyValue(UnitTestSupport.PROP_UI_RELEVANT_TESTS);
        assertThat(updatedUiTestsToExecute, is("GamesOverviewPageIntegrationTest, MyTestIntegrationTest"));

    }

    @Test
    public void testAutoSaveWithReset() throws ConfigurationException, org.apache.commons.configuration2.ex.ConfigurationException {
        final File file = UnitTestSupport.getFileByName(TEST_EXECUTION_PROPERTIES);
        final FileBasedConfigurationBuilder<PropertiesConfiguration> builder = new FileBasedConfigurationBuilder<PropertiesConfiguration>(PropertiesConfiguration.class).configure(new FileBasedBuilderParametersImpl().setFile(file));
        final PropertiesConfiguration config1 = builder.getConfiguration();
        builder.setAutoSave(true);
        builder.resetResult();
        final PropertiesConfiguration config2 = builder.getConfiguration();
        assertNotSame("No new configuration created", config1, config2);
        config2.setProperty("test", 1);
        config1.setProperty("test2", 2);
    }

    public static List<String> provideGitDiffList() {
        final List<String> diffList = new ArrayList<String>();
        diffList.add("de.ee.optimization.game.entity.GameRowData");
        diffList.add("de.ee.optimization.game.entity.GameProducer");
        return diffList;
    }

}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme