package de.ee.selectiveexec.boundary;

import de.ee.selectiveexec.entity.TestLevel;
import org.junit.Test;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Lisa Stolz
 */
public class SelectiveExecutionPropertiesProviderTest {

    private static final TestLevel level = TestLevel.API_TEST;

    @Test
    public void testGetFileName() throws Exception {
        final String result = SelectiveExecutionPropertiesProvider.getFileName(level);
        assertThat(result, is("cucumber-result-mapping.xml"));
    }

    @Test
    public void testGetPath() throws Exception {
        final String result = SelectiveExecutionPropertiesProvider.getPath(level);
        assertThat(result, is("src/main/resources/coverage-reports"));
    }

    @Test
    public void testGetPathToFile() throws Exception {
        final String result = SelectiveExecutionPropertiesProvider.getPathToFile(level);
        assertThat(result, is("src/main/resources/coverage-reports/cucumber-result-mapping.xml"));
    }

    @Test
    public void testGetTestExecutionList_needKeyIsTrue_receivedKey() throws Exception {
        final String result = SelectiveExecutionPropertiesProvider.getTestExecutionList(level, true);
        assertThat(result, is("api.tests.to.execute"));
    }
    
    @Test
    public void testGetDefaultTestExecutionList_needKeyIsTrue_receivedKey() throws Exception {
        final String result = SelectiveExecutionPropertiesProvider.getDefaultTestExecutionList(level, true);
        assertThat(result, is("api.tests.to.execute.default"));
    }

    @Test
    public void testGetDefaultTestExecutionList_needKeyIsFalse_receivedValue() throws Exception {
        final String result = SelectiveExecutionPropertiesProvider.getDefaultTestExecutionList(level, false);
        assertThat(result, is("src/test/resources/features/"));
    }

    public void printProperties() {
        //SelectiveExecutionPropertiesProvider.loadProperties().forEach((k, v) -> System.out.println(("KEY: " + k + ":" + " VALUE: " + v)));
    }
}

// Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme