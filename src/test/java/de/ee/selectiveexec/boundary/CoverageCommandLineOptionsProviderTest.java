package de.ee.selectiveexec.boundary;

import de.ee.selectiveexec.entity.CoverageCommandLineOptions;
import de.ee.selectiveexec.entity.OptionPair;
import org.junit.Test;

import java.util.List;
import java.util.Properties;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

/**
 * @author Lisa Stolz
 */
public class CoverageCommandLineOptionsProviderTest {

    @Test
    public void testLoadProperties() throws Exception {
        final Properties result = CoverageCommandLineOptionsProvider.loadProperties();
        assertThat(result.get("append").toString(), is("false"));
    }

    @Test
    public void testGetCoverageCommandLineOptions() throws Exception {
        final CoverageCommandLineOptions result = CoverageCommandLineOptionsProvider.getCoverageCommandLineOptions();
        assertThat(result, is(notNullValue()));
    }

    @Test
    public void testGetOptionsPrepare() throws Exception {
        final List<String> result = CoverageCommandLineOptionsProvider.getOptionsPrepare();
        assertThat(result.size(), is(3));
        assertThat(result.get(0), is("port=10001"));
    }

    @Test
    public void testCreateOption() throws Exception {
        final OptionPair result = CoverageCommandLineOptionsProvider.createOption("option", "value");
        assertThat(result.getOption(), is("option"));
        assertThat(result.getValue(), is(" --option value"));

    }

    @Test
    public void testCommandLineOptions() throws Exception {
        final CoverageCommandLineOptions result = getCoverageCmdOptions();
        assertThat(result, is(notNullValue()));
    }

    private static CoverageCommandLineOptions getCoverageCmdOptions() {
        return CoverageCommandLineOptionsProvider.getCoverageCommandLineOptions();
    }

    private static void printCoverageCmdOptions() {
        final CoverageCommandLineOptions configs = CoverageCommandLineOptionsProvider.getCoverageCommandLineOptions();
        System.out.println(configs.getAddress().getOption() + "    value: " + configs.getAddress().getValue());
        System.out.println(configs.getClassFiles().getOption() + "    value: " + configs.getClassFiles().getValue());
        System.out.println(configs.getCsv().getOption() + "    value: " + configs.getCsv().getValue());
        System.out.println(configs.getHtml().getOption() + "    value: " + configs.getHtml().getValue());
        System.out.println(configs.getSourceFiles().getOption() + "    value: " + configs.getSourceFiles().getValue());
        System.out.println(configs.getPort().getOption() + "    value: " + configs.getPort().getValue());
        System.out.println(configs.getName().getOption() + "    value: " + configs.getName().getValue());
        System.out.println(configs.getDestFile().getOption() + "    value: " + configs.getDestFile().getValue());
        System.out.println(configs.getAppend().getOption() + "    value: " + configs.getAppend().getValue());

    }
}

//Generated with love by TestMe :) Please report issues and submit feature requests at: http://weirddev.com/forum#!/testme