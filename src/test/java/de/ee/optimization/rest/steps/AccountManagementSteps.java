package de.ee.optimization.rest.steps;

import de.ee.optimization.game.entity.GamerAccount;
import de.ee.optimization.integrationtest.control.AccountManagementControl;
import io.cucumber.java.After;
import io.cucumber.java.Before;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

public class AccountManagementSteps {

    private static final String USER_FIRST_NAME = "newCustomersFirstName";
    private static final String USER_LAST_NAME = "newCustomersLastName";
    private static final String ACCOUNT_NAME = "newCustomersAccountName";
    private static final String MAIL_ADDRESS = "newCustomersMail";

    private final AccountManagementControl accountManagementControl;


    public AccountManagementSteps(final AccountManagementControl accountManagementControl) {
        this.accountManagementControl = accountManagementControl;
    }

    @Given("a new user wants to create an account registration")
    public void a_new_user_wants_to_create_an_account_registration() {
        accountManagementControl.saveRegistration(USER_FIRST_NAME, USER_LAST_NAME, MAIL_ADDRESS, ACCOUNT_NAME);

    }

    @When("the request reaches our service the user is verified")
    public void the_request_reaches_our_service_the_user_is_verified() {
        accountManagementControl.verifyRegistration(MAIL_ADDRESS);
        final GamerAccount account = createUserAccount();
        assertThat(account.getUserId(), is(ACCOUNT_NAME));
    }

    @Then("the account was successfully created")
    public void the_account_was_successfully_created() {
        assertThat(accountManagementControl.findGamerAccountByName(ACCOUNT_NAME), is(notNullValue()));

    }


    @After("@cleanup")
    public void tearDown() {
        accountManagementControl.deleteUser(MAIL_ADDRESS);
    }

    @Before("@cleanup")
    public void setUp() {
        accountManagementControl.deleteUser(MAIL_ADDRESS);
    }


    private GamerAccount createUserAccount() {
        return accountManagementControl.createGamerAccount(USER_FIRST_NAME, USER_LAST_NAME, MAIL_ADDRESS, ACCOUNT_NAME);
    }

}
