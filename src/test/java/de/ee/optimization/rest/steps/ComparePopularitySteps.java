package de.ee.optimization.rest.steps;

import de.ee.optimization.integrationtest.control.ComparePopularityControl;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;

public class ComparePopularitySteps {


    private final ComparePopularityControl comparePopularityControl;


    public ComparePopularitySteps(final ComparePopularityControl comparePopularityControl) {
        this.comparePopularityControl = comparePopularityControl;
    }

    @Given("a list of games to compare popularity")
    public void a_new_user_wants_to_create_an_account_registration() {

    }

    @When("i request to get the popularity report")
    public void the_request_reaches_our_service_the_user_is_verified() {

        final String message = comparePopularityControl.getComparisonMessage();
        assertThat(message, is("Information about the most popular Games in 2021!"));

    }

    @Then("i will see the report")
    public void the_account_was_successfully_created() {

    }


}
