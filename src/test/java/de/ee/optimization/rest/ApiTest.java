package de.ee.optimization.rest;

import io.cucumber.junit.Cucumber;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;

import java.io.IOException;

/**
 * The run listener for cucumber tests can be configured in the build section of pom.xml
 * by specifying the listener for the maven-failsafe-plugin,
 *
 * @CucumberOptions (plugin = { " pretty ", " html : target / cucumber " }, features = " src / test / resources / features / ",
 *glue = " de.ee.optimization.rest.steps ")
 */
@RunWith(Cucumber.class)

public class ApiTest {
    @BeforeClass
    public static void setUp() throws IOException {
        //maybe later
    }

    @AfterClass
    public static void tearDown() throws IOException {
        //later delete test data
    }

}
