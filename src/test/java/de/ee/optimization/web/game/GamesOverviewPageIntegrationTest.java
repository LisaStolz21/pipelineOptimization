package de.ee.optimization.web.game;


import de.ee.optimization.game.entity.Platform;
import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import primefaces.GamesSupport;
import primefaces.PrimefacesSupport;
import primefaces.SeleniumSupport;
import primefaces.WelcomePageNav;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class GamesOverviewPageIntegrationTest {

    protected static WebDriver driver;

    public static GamesPage gamesPage;

    @BeforeClass
    public static void setUp() throws InterruptedException {
        driver = SeleniumSupport.initDriver();
        WelcomePageNav.selectPage("Games", driver);
        gamesPage = PageFactory.initElements(driver, GamesPage.class);
    }

    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }

    @Test
    public void tableTest() {
        final WebElement webElementTable = gamesPage.getGamesTable();
        assertThat(webElementTable, is(notNullValue()));
        GamesSupport.selectPlatform(driver, webElementTable, "Minecraft", Platform.XBOX);
    }

    @Test
    public void selectGameAndSubmitSelection() {
        final WebElement webElementTable = gamesPage.getGamesTable();
        assertThat(webElementTable, is(notNullValue()));
        GamesSupport.selectPlatform(driver, webElementTable, "Minecraft", Platform.XBOX);
        GamesSupport.selectGame(webElementTable, "Minecraft");
        PrimefacesSupport.click(gamesPage.getBtnSubmit(), true);
        assertThat(gamesPage.getMessage().getText(), is("We received your Game selection: Game: Minecraft Platform: " +
                "XBOX"));
        assertThat(gamesPage.getMessage().getText(), is(notNullValue()));
    }

}
