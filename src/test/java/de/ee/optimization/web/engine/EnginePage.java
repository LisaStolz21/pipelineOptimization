package de.ee.optimization.web.engine;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

public class EnginePage {

    private final WebDriver driver;

    //    public static final String URL = "http://localhost:8080/pipelineOptimization/jsf/pages/engineKnowledgePage" +
//            ".jsf";
    public static final String ENGINE_FORM = "engineForm:";
    public static final String ENGINE_ACCORDION = "engineAccordion:";

    public EnginePage(final WebDriver driver) {
        this.driver = driver;
    }

    @FindBy(id = ENGINE_FORM + "engineAccordion")
    private WebElement engineAccordion;

    public WebElement getEngineAccordion() {
        return engineAccordion;
    }
}
