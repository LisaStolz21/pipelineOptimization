package de.ee.optimization.web.engine;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runners.MethodSorters;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import primefaces.PrimefacesSupport;
import primefaces.SeleniumSupport;
import primefaces.WelcomePageNav;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;

@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class EngineKnowledgePageIntegrationTest {

    protected static WebDriver driver;
    public static EnginePage enginePage;

    @BeforeClass
    public static void setUp() throws InterruptedException {
        driver = SeleniumSupport.initDriver();
        WelcomePageNav.selectPage("Engine", driver);
        enginePage = PageFactory.initElements(driver, EnginePage.class);
    }

    @AfterClass
    public static void quitDriver() {
        driver.quit();
    }

    @Test
    public void accordionPanelTest() {
        final WebElement engineAccordion = enginePage.getEngineAccordion();
        assertThat(engineAccordion, is(notNullValue()));
    }

    @Test
    public void openAccordionHeaderAndCheckContentTest() {
        final WebElement engineAccordion = enginePage.getEngineAccordion();
        assertThat(engineAccordion, is(notNullValue()));
        PrimefacesSupport.openAccordionHeaderByIndex(driver, engineAccordion, 1);
        final WebElement accordionContent = PrimefacesSupport.getAccordionContentByIndex(engineAccordion, 1);
        assertThat(accordionContent, is(notNullValue()));
        assertThat(accordionContent.getText(), is("[Fortnite, Gears of War, BioShock]"));

    }
}
