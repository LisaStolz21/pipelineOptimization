package de.ee.optimization.game.boundary;

import de.ee.optimization.game.entity.Game;
import de.ee.optimization.game.entity.GameRowData;
import de.ee.optimization.game.entity.GamesGenre;
import de.ee.optimization.game.entity.PlatformPackage;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.time.LocalDateTime;
import java.util.Calendar;
import java.util.Collections;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;

import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.CoreMatchers.notNullValue;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.when;

public class GamesControllerTest {
    @Mock
    GamesBF gamesBF;
    //Field localDateTime of type LocalDateTime - was not mocked since Mockito doesn't mock a Final class when
    // 'mock-maker-inline' option is not set
    @Mock
    List<Game> listedGames;
    @Mock
    List<GameRowData> rowDataInput;
    @InjectMocks
    GamesController gamesController;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    public void testInit() throws Exception {

        final Game myFirstGame = createGame("testDev", "testName", "engine", GamesGenre.ACTION,
                PlatformPackage.PC_PS_XBOX_NS);
        myFirstGame.setRelease(new Date());
        when(gamesBF.findAllGames()).thenReturn(Collections.singletonList(myFirstGame));

        gamesController.init();
    }

    @Test
    public void testGetReleaseDate() throws Exception {
        final Date date = new GregorianCalendar(2021, Calendar.OCTOBER, 9, 23, 7).getTime();
        final String result = gamesController.getReleaseDate(date);
        assertThat(result, is(notNullValue()));
    }

    @Test
    public void testGetPlatformsString() throws Exception {
        final List<String> result = gamesController.getPlatformsString(PlatformPackage.PC);
        assertEquals(Collections.singletonList("PC"), result);
    }

    @Test
    public void testGetLocalDateTimeString() throws Exception {
        gamesController.localDateTime = LocalDateTime.now();
        final String result = gamesController.getLocalDateTimeString();
        assertThat(result, is(notNullValue()));
    }

    final Game createGame(final String developer, final String name, final String engine, final GamesGenre gamesGenre
            , final PlatformPackage platformPackage) {
        final Game newGame = new Game();
        newGame.setDeveloper(developer);
        newGame.setEngine(engine);
        newGame.setGamesGenre(gamesGenre);
        newGame.setName(name);
        newGame.setPlatform(platformPackage);
        return newGame;
    }
}

//Generated with love by TestMe :)