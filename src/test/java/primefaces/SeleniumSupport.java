package primefaces;

import de.ee.optimization.EnvironmentConfiguration;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;

import static de.ee.optimization.EnvironmentConfiguration.Environment;
import static de.ee.optimization.EnvironmentConfiguration.PROP_WEBDRIVER;
import static de.ee.optimization.EnvironmentConfiguration.getSystemProperty;

/**
 * support Class for web-ui testing
 */
public class SeleniumSupport {

    public static WebDriver initDriver() {
        System.setProperty("webdriver.chrome.driver", getSystemProperty(PROP_WEBDRIVER));
        final ChromeOptions chromeOptions = new ChromeOptions();
        chromeOptions.addArguments("--headless");
        if (Environment.PROD.equals(EnvironmentConfiguration.getEnvironment())) {
            chromeOptions.addArguments("--no-sandbox");
            chromeOptions.setBinary("/usr/bin/google-chrome");
        }
        return new ChromeDriver(chromeOptions);
    }

}
