package primefaces;

import de.ee.optimization.EnvironmentConfiguration;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

public class WelcomePageNav {

    public static final String OVERVIEW_BTN = "welcomeForm:overviewBtn";
    public static final String INFO_BTN = "welcomeForm:informationBtn";

    public static void selectPage(final String pageName, final WebDriver driver) throws InterruptedException {
        driver.get(EnvironmentConfiguration.getSystemProperty(EnvironmentConfiguration.PROP_WEB_URL));
        if (pageName.equals("Games")) {
            final WebElement e = driver.findElement(By.id(OVERVIEW_BTN));
            e.click();
        } else if (pageName.equals("Engine")) {
            final WebElement e = driver.findElement(By.id(INFO_BTN));
            e.click();
        }

    }

}
