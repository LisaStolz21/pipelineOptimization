package testsupport;

import de.ee.selectiveexec.control.FileSupport;
import org.apache.commons.configuration2.PropertiesConfiguration;
import org.apache.commons.configuration2.builder.FileBasedBuilderParametersImpl;
import org.apache.commons.configuration2.builder.FileBasedConfigurationBuilder;
import org.apache.commons.configuration2.ex.ConfigurationException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

/**
 * @author Lisa Stolz
 */
public class UnitTestSupport {

    public static final String TEST_DATA_DIRECTORY_PATH = "src/test/resources/test-data/";
    public static final String TEST_EXECUTION_PROPERTIES = "integrationtest-execution-test.properties";
    public static final String TEST_COVERAGE_MAPPING = "integrationtest-result-mapping-test.xml";
    public static final String PROP_UI_RELEVANT_TESTS = "web.tests.to.execute";
    public static final String PROP_API_RELEVANT_TESTS = "api.tests.to.execute";

    public static String getPathToFile(final String fileName) {
        return TEST_DATA_DIRECTORY_PATH + fileName;
    }

    public static File getFileByName(final String fileName) {
        return FileSupport.getFileIfExists(getPathToFile(fileName));
    }

    public static Properties loadTestExecutionProperties() {
        try {
            final Properties properties = new Properties();

            final File file = new File(getPathToFile(TEST_EXECUTION_PROPERTIES));
            properties.load(new FileInputStream(file));
            return properties;
        } catch (final IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static String findPropertyValue(final String key) {
        final Properties properties = UnitTestSupport.loadTestExecutionProperties();
        return properties.get(key).toString();
    }

    public static void setEntryTestPropertyFile(final String key, final String value) {
        final FileBasedConfigurationBuilder<PropertiesConfiguration> builder = getConfigurationBuilder();
        try {
            final PropertiesConfiguration config = builder.getConfiguration();
            config.setProperty(key, value);
            builder.resetResult();
            builder.save();
        } catch (final ConfigurationException io) {
            io.printStackTrace();
        }
    }

    private static FileBasedConfigurationBuilder<PropertiesConfiguration> getConfigurationBuilder() {
        final File file = getFileByName(TEST_EXECUTION_PROPERTIES);
        return new FileBasedConfigurationBuilder<PropertiesConfiguration>(PropertiesConfiguration.class)
                .configure(new FileBasedBuilderParametersImpl().setFile(file));
    }
}
