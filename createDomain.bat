echo on

set AS_JAVA=C:\Users\SLZL\Documents\Bachelorarbeit\zulu-ca-jdk8.0.312
set JAVA_HOME=C:\Users\SLZL\Documents\Bachelorarbeit\zulu-ca-jdk8.0.312
set "glassfishDir=C:\Users\SLZL\Documents\Bachelorarbeit\payara-5.2021.10\glassfish"

set "domainName=selectiveExecution"
set "domainDir=%glassfishDir%\domains\%domainName%"

call %glassfishDir%\bin\asadmin create-domain --nopassword %domainName%
copy postgresql-42.2.18.jar %domainDir%\lib\ext
copy org.jacoco.agent.jar %domainDir%\lib\ext

call %glassfishDir%\bin\asadmin start-domain %domainName%

call %glassfishDir%\bin\asadmin create-jvm-options -Denvironment=LOCAL

call %glassfishDir%\bin\asadmin create-jdbc-connection-pool --datasourceclassname org.postgresql.ds.PGSimpleDataSource --restype javax.sql.DataSource --driverclassname org.postgresql.Driver --property user=postgres:password=postgres:url="jdbc\:postgresql\://localhost\:5432/postgres" PIPELINE_JDBC

call %glassfishDir%\bin\asadmin ping-connection-pool PIPELINE_JDBC

call %glassfishDir%\bin\asadmin create-jdbc-resource --connectionpoolid PIPELINE_JDBC jdbc/pipelineopt

copy log4j2.xml %domainDir%\lib\classes

call %glassfishDir%\bin\asadmin stop-domain %domainName%

:End